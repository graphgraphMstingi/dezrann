


# Dezrann &ndash; Interactive music analysis on the web


This is the preliminary documentation for technical usage of **Dezrann**.

[Dezrann](http://www.dezrann.net) is an open platform to *study, hear, and annotate music on the web*.

Dezrann usages include *music education*
as well as corpus annotation for *musicology or computer music research*.


# Déploiement de Dezrann

## Déploiement du front

- se placer dans le répertoire `/code/dez-client/dezrann`
- lancer `polymer build`
- les fichiers de prod sont générés dans `/code/dez-client/dezrann/build/default/`
- copier dans ce repertoire le fichier config-public.json en le renommant config.json
- pour le prototype de collaboration temps réel: copier aussi `collab.html`

## Déploiement des backend

Actuellement les fichiers sources (*.js) ainsi que les dépendances (node_modules) sont simplement recopiés sur le serveur (scp). Aucun scripts n'est utilisé pour le moment. Les fonctionnalité de build de `npm` sont à étudier. webpack?

## Déploiement de corpus

Les fichiers doivent être recopié sur le web service (`dez-ws`) http://dezws.lifl.fr.

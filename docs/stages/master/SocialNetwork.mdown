Un reseau social pour l'annotation de pièces musicales

L'équipe Algomus développe l'application web 'Dezrann' pour lire et annoter
des partitions musicales (prototype sur http://dezrann.net/). L'annotation se
fait en ajoutant des éléments graphiques sur la partition: les 'labels'. Ces
annotations permettre de faire une analyse structurelle, harmonique,
rythmique ou encore instrumentale.
Cette application utilise le framework Vue et est écrite en TypeScript.
Cette application est un véritable service permettant d'accéder à plusieurs
corpus de plusieurs centaines de
pièces musicales. Certaines de ces pièces ne sont accessibles que par des
utilisteurs possédant un compte.

Le projet a pour objectif de faciliter la communication entre les différents
utilisateurs pour faire de l'application un véritable espace collaboratif
d'analyse musicale. A l'instar de Soundcloud (https://soundcloud.com/) qui
permet à chacun d'écouter un morceau de musique et d'y laisser ses impressions,
cette nouvelle version de dezrann permettra à un utilisateur de proposer une
analyse d'une pièce musicale.
Plus précisément les fonctionnalités que nous souhaitons ajouter sont les
suivantes:
- espace de travail et de gestion des pièces et analyses d'un utilisateur
- partager une analyse à un ou plusieurs collaborateur en lecture ou en
écriture
- proposer une nouvelle version d'une analyse existante
- modifier l'analyse d'un collaborateur
- rendre une analyse publique

Le code sera écrit avec grand soin, documenté et testé.

Mots clés : partition musicales, Vue, typescript.

Liens :
 - http://www.dezrann.net
 - https://soundcloud.com/



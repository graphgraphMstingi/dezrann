

# `dez-ws` API

The Dezrann web service `dez-ws` answers to some API calls.
The links below use the public `dez-ws` server.

## `/corpus`

[/corpus](http://dezws.lifl.fr/corpus) returns all publicly available pieces.
When the user is logged in, this list may contain additional pieces that the user can access.

```json
{
  "id": "corpus",
  "pieces": [
    {
      "composer": "J.-S. Bach",
      "id": "bwv847"
      ...
    }
    ...
  ]
}
```

The `pieces` list each piece. Each piece has an `id`, here `bwv847`.

## Pieces

### `/corpus/id`

[/corpus/bwv847](http://dezws.lifl.fr/corpus/bwv847) retrieves the `info.json` file on that piece (see <info-json.md>)
The `dez-ws` server adds the following informations:

```json
{  
  "permissions": [
    "read",
    "update-analysis"
  ]
}
```

These `permissions` elements  describes the permissions granted on the current user and are generated from `access.json` files.

### Implicit position and images

As described in [infos-json.md](infos-json.md), the calls
[sources/images/bwv847/image](http://dezws.lifl.fr/corpus/bwv847/sources/images/bwv847/image)
and [sources/images/bwv847/positions](http://dezws.lifl.fr/corpus/bwv847/sources/images/bwv847/positions)
retrieve the images and positions files.

## Analyses


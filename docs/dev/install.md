

# Installation and running instructions

- install [npm](<https://github.com/nodesource/distributions/blob/master/README.md>)
- install some packages: `npm install -g bower polymer-cli gulp-cli`
- setup `gulp` with `npm install \--save-dev gulp gulp-rm-lines`
- run `gulp` in `code/`

A minimal local installation requires:

- either `dez-client` pointing towards the public servers (you need to be on the same branch *TODO* than the public servers)
- or `dez-client` pointing towards a local `dez-ws`

 
## dez-ws 

In `code/dez-server/``

- create `config.json` by copying `config-sample.json`
- run `npm start`
- the server now answer, as for example <http://localhost:8888/ping> and to <http://localhost:8888/corpus>

## dez-auth (optional)

## dez-collab (optional)

## dez-client

In `code/dez-client/dezrann`:

- create `config.json`, for example by copying either `config-local.json` or `config-public.json`
- run `bower install` or `bower update` (possibly after `rm -rf bower_components`)
- run `polymer serve`
- the client should be available from <http://localhost:8081/> or another address given by polymer


/*
  This file is part of Dezrann <http://www.dezrann.net>
  Copyright (C) 2016-2021 by Algomus Team at CRIStAL (UMR CNRS 9189, Université Lille),
  in collaboration with MIS (UPJV, Amiens) and LITIS (Université de Rouen).

  Dezrann is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Dezrann is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Dezrann.  If not, see <https://www.gnu.org/licenses/>.
*/


let express = require('express');
let app = express();
let corpus = require('./corpus');
let bodyParser = require('body-parser')
let jwt = require('jsonwebtoken')
let fs = require('fs');
const config = require('./config.json');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use((req, res, next) => {
  if (req.headers.authorization) {
    let token = req.headers.authorization.split('Bearer ')[1]
    try {
      let data = jwt.verify(token, fs.readFileSync(config.publicKey, 'utf8'), { algorithm: ["RS256"] })
      req.user = {
        uid : data.sub,
        groups : data.groups
      }
    } catch (e) {
      res.status(401).send(e)
      return
    }
  } else {
    req.user = {
      uid : "public",
      groups : []
    }
  }
  next()
})

app.get('/ping', (req, res) => {
  res.status(200).send("pong")
})

app.get('*/:piece/sources/images/:imageName/positions', corpus.imagePositions)
app.get('*/:piece/sources/images/:imageName/file', corpus.imageFile)
app.get('*/:piece/sources/audios/:audioName/file', corpus.audioFile)
app.get('*/:piece/sources/audios/:audioName/synchro', corpus.audioSynchro)
app.get('*/:piece/sources/audios/:audioName/images/:imageName/positions', corpus.audioImagePositions)
app.get('*/:piece/sources/audios/:audioName/images/:imageName/file', corpus.audioImageFile)
app.get('*/:piece/analyses', corpus.analyses)
app.post('*/:id/analyses', corpus.createAnalysis)
app.get('*/:piece/analyses/:name', corpus.analysis)
app.get('*/:id/recursive', corpus.recursive)
app.get('*/:id', corpus.flat)

module.exports = app;

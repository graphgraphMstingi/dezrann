function formatImageFilesNames (image, name) {
  image.image = "sources/images/" + name + "/file"
  if (possiblePositionsFileName(image.positions, name))
    image.positions = "sources/images/" + name + "/positions"
}

function formatAudioFilesNames (audio, name) {
  audio.file = "sources/audios/" + name + "/file"
  audio["onset-date"] = "sources/audios/" + name + "/synchro"
}

function prefixForShortFileName (source) {
  let re = new RegExp('^(.+)(' + source.extensions.join('|') + ')$')
  let match = re.exec(source.filename)
  if (match) return match[1]
  return ""
}

function formateFilesNames(source) {
  let name = prefixForShortFileName(source)
  if (name != "")
    source.formatFilesNames(name)
}

class PieceDataFormater {

  constructor (validator) {
    this._validator = validator
  }

  _validateJsonFormat (input) {
    if (input == undefined) throw "EMPTY_PIECE_DATA"
    if (Array.isArray(input)) throw "BAD_PIECE_FORMAT"
    if (!this._validator.validates(input, schema)) throw "BAD_PIECE_FORMAT"
  }

  _prefixForShortFileName (source) {
    if (source.filename.substring(0, 4) == "http") return ""
    let re = new RegExp('^(.+)(' + source.extensions.join('|') + ')$')
    let match = re.exec(source.filename)
    if (match) return match[1]
    return ""
  }

  _formateFilesNames(source) {
    let name = this._prefixForShortFileName(source)
    if (name != "")
      source.formatFilesNames(name)
  }

  formate (input) {
    this._validateJsonFormat(input)
    if (input.sources.images) {
      input.sources.images.forEach(image => {
        this._formateFilesNames(new Image(image))
      })
    }
    if (input.sources.audios) {
      input.sources.audios.forEach(audio => {
        this._formateFilesNames(new Audio(audio))
      })
    }
    return input
  }

}

class Audio {

  constructor (data) {
    this._data = data
    this.filename = data.file
    this.extensions = [".mp3",".ogg",".wav",".mid"]
  }

  formatFilesNames (name) {
    // if yt-id exists file is only used to have a name...
    // in this case file entry must be deleted to be compatible with the client
    if (this._data["yt-id"]) {
      delete this._data.file
    } else {
      this._data.file = "sources/audios/" + name + "/file"
    }
    this._data["onset-date"] = "sources/audios/" + name + "/synchro"
    if (this._data.images) {
      this._data.images.forEach(image => {
        formateFilesNames(new Image(image, "sources/audios/" + name + "/images/"))
      })
    }
  }

}

class Image {

  constructor (data, prefix = "sources/images/") {
    this._data = data
    this._prefix = prefix
    this.filename = data.image
    this.extensions = [".jpg",".png",".svg"]
  }

  formatFilesNames (name) {
    this._data.image = this._prefix + name + "/file"
    if (this._possiblePositionsFileName(this._data.positions, name))
      this._data.positions = this._prefix + name + "/positions"
  }

  _possiblePositionsFileName (name, imageName) {
    return name == undefined
      || name == "positions.json"
      || name == imageName + ".json"
      || name == imageName + "-pos.json"
      || name == "positions-" + imageName + ".json"
  }

}

const schema = {
  properties : {
    id : { type : "string" },
    composer : { type : "string" },
    title : { type : "string" },
    "time-signature" : { type : "string" },
    "last-offset" : { type : "number" },
    "title:fr" : { type : "string" },
    editor : { type : "string" },
    sources : {
      type : "object",
      properties : {
        images : {
          type : "array",
          items : { $ref : "#/definitions/image" },
          minItems : 1
        },
        audios : {
          type : "array",
          items : { $ref : "#/definitions/audio" },
          // minItems : 1
        }
      },
      anyOf : [ { required : [ "images" ] }, { required : [ "audios" ] } ]
    }
  },
  required : ["id", "sources"],
  minProperties: 2,
  definitions : {
    image : {
      type : "object",
      properties : {
        image : {
          type : "string",
          pattern : "^(sources\/images\/[^\/]+\/file)$|^(.+)(.jpg|.png|.svg)$|^http[s]*:\/\/.*$"
        },
        type : { type : "string" },
        name : { type : "string" },
        info : { type : "string" },
        positions : {
          type : "string",
          pattern : "^(sources\/images\/[^\/]+\/positions)$|^([^\/]+).json$|^http[s]*:\/\/.*$"
        }
      },
      required : [ "image", "type"]
    },
    audioImage : {
      type : "object",
      properties : {
        image : {
          type : "string",
          pattern : "^(sources\/audios\/[^\/]+\/images\/[^\/]+\/file)$|^(.+)(.jpg|.png|.svg)$|^http[s]*:\/\/.*$"
        },
        type : { type : "string" },
        name : { type : "string" },
        info : { type : "string" },
        positions : {
          type : "string",
          pattern : "^(sources\/audios\/[^\/]+\/images\/[^\/]+\/positions)$|^(.+).json$|^http[s]*:\/\/.*$"
        }
      },
      required : [ "image", "type"]
    },
    audio : {
      type : "object",
      properties : {
        file : {
          type : "string",
          pattern : "^(sources\/audios\/[^\/]+\/file)$|^(.+)(.mp3|.ogg|.wav|.mid)|^http[s]*:\/\/.*$"
        },
        "yt-id" : { type : "string" },
        "onset-date" : {
          type : "string",
          pattern : "^(sources\/audios\/[^\/]+\/file)$|^(.+).json|^http[s]*:\/\/.*$"
        },
        images : {
          type : "array",
          items : { $ref : "#/definitions/audioImage" },
        },
        info : { type : "string" }
      },
      required : [ "file", "onset-date"]
    }
  }
};

module.exports = PieceDataFormater;

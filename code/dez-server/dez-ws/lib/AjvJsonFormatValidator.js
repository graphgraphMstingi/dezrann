const Ajv = require('ajv');

class AjvJsonFormatValidator {

  constructor () {
    this._ajv = new Ajv()
  }

  validates (input, schema) {
    const validate = this._ajv.compile(schema)
    const valid = validate(input)
    if (!valid) console.log(validate.errors)
    return valid
  }

}

module.exports = AjvJsonFormatValidator

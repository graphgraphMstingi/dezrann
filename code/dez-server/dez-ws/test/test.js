const request = require('supertest');
const chai = require("chai")
const app = require('../app');
const expect = chai.expect

const CORPUS_URL = "/corpus"
const SUB_CORPUS_URL = CORPUS_URL + "/bach/fugues"
const PIECE_URL = "/bwv847"
const PIECE_IMAGE_URL = PIECE_URL + '/sources/images/bwv847'
const PIECE_AUDIO_URL = PIECE_URL + '/sources/audios/bwv847'
const PIECE_AUDIO_IMAGE_URL = PIECE_AUDIO_URL + '/images/bwv847-waves'
const PIECE_FLAT_URL = "/bwv848"
const PIECE_FLAT_IMAGE_URL = PIECE_FLAT_URL + '/sources/images/bwv848'
const PIECE_FLAT_AUDIO_URL = PIECE_FLAT_URL + '/sources/audios/bwv848'
const PIECE_FLAT_AUDIO_IMAGE_URL = PIECE_FLAT_AUDIO_URL + '/images/bwv848-waves'
const SUB_CORPUS_PIECE_URL = CORPUS_URL + "/bach/choirs/bwv269"
const SUB_CORPUS_PIECE_ANALYSIS_URL = SUB_CORPUS_PIECE_URL + '/analyses/riemenschneider001.dez'
const PROTECTED_CORPUS_URL = CORPUS_URL + "/algomus"
const PROTECTED_BY_CORPUS_PIECE_URL = PROTECTED_CORPUS_URL + '/mozart/quartets/k155'
const PROTECTED_PIECE_URL = CORPUS_URL + '/bwv849'
const PROTECTED_ANALYSES_LIST_URL = CORPUS_URL + '/bwv848/analyses'
const PROTECTED_BY_CORPUS_ANALYSIS_URL = PROTECTED_BY_CORPUS_PIECE_URL + '/analyses/analysis.dez'
const PROTECTED_BY_PIECE_ANALYSIS_URL = PROTECTED_PIECE_URL + '/analyses/04-reference.dez'
const PROTECTED_ANALYSIS_NAME = 'computed.dez'
const PROTECTED_USER_CORPUS_URL = CORPUS_URL + "/laurentcorpus"
const PROTECTED_USER_BY_CORPUS_PIECE_URL = PROTECTED_USER_CORPUS_URL + '/k155'
const PROTECTED_USER_PIECE_URL = CORPUS_URL + '/laurentpiece'
const PROTECTED_USER_ANALYSES_LIST_URL = CORPUS_URL + '/bwv850/analyses'
const PROTECTED_USER_BY_CORPUS_ANALYSIS_URL = PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses/analysis.dez'
const PROTECTED_USER_BY_PIECE_ANALYSIS_URL = PROTECTED_USER_PIECE_URL + '/analyses/analysis.dez'

const ADMIN_JWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbiIsIm5hbWUiOiJhZG1pbiIsImFkbWluIjp0cnVlLCJpYXQiOjE1MTYyMzkwMjIsImdyb3VwcyI6ImFkbWluIn0.YhjB25GTkMfAh3qqIO1QBEGPO9Q6iOqEjREmRD2OrCZ5o-99hV0fJGcHmX3kw0N_ONv54NhjviyqKSOAVpfF3g"
const ALGOMUS_MEMBER_JWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJsYXVyZW50IiwibmFtZSI6ImxhdXJlbnQiLCJpYXQiOjE1MTYyMzkwMjIsImdyb3VwcyI6WyJhbGdvbXVzIl19.L0V_nqL-urVZ22o1cUE3Ywj7uCPv0OmZNGLnz6K6gFqqjLbbW19hb8c-W5OWxaHv-wG1WsO-nrDoUmCm4_tV-g"
const BAD_GROUP_JWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyIiwibmFtZSI6InVzZXIiLCJpYXQiOjE1MTYyMzkwMjIsImdyb3VwcyI6WyJncm91cCJdfQ.XiyitM5Lo9DsLuxC25R8__2Z7zMSOciuPE8t335NdjiOm3hyBdJjPrDWWCwQ0dAhFoM__p70qP3zNKxw8hQqog"

function checkEndPoint (url, done) {
  request(app)
  .get(url)
  .set('Accept', 'application/json')
  .expect('Content-Type', /json/)
  .expect(200, done)
}

function checkAuthEndPoint (url, token, done) {
  request(app)
  .get(url)
  .set('Accept', 'application/json')
  .set('Authorization', 'Bearer ' + token)
  .expect(200, done)
}

function check404 (url, done) {
  request(app)
  .get(url)
  .set('Accept', 'application/json')
  .expect(404, done)
}

describe('GET CORPUS', () => {
  it('should return a 404 if corpus (or piece) does not exist', done => {
    check404(CORPUS_URL + '/unknown', done)
  });
  it('should return the corpus', done => {
    checkEndPoint(CORPUS_URL, done)
  });
  it('should return the corpus to the admin user', done => {
    checkAuthEndPoint(CORPUS_URL, ADMIN_JWT, done)
  });
  it('should return the corpus to an authenticated user', done => {
    checkAuthEndPoint(CORPUS_URL, ALGOMUS_MEMBER_JWT, done)
  });
  it('should return the corpus recursively', done => {
    checkEndPoint(CORPUS_URL + '/recursive', done)
  });
  it('should return a sub corpus', done => {
    checkEndPoint(SUB_CORPUS_URL, done)
  });
  it('should return the sub corpus to the admin user', done => {
    checkAuthEndPoint(SUB_CORPUS_URL, ADMIN_JWT, done)
  });
  it('should return the sub corpus to an authenticated user', done => {
    checkAuthEndPoint(SUB_CORPUS_URL, ALGOMUS_MEMBER_JWT, done)
  });
  it('should return a sub corpus recursively', done => {
    checkEndPoint(SUB_CORPUS_URL + '/recursive', done)
  });
});

describe('GET PIECE', () => {
  it('should return piece infos', done => {
    checkEndPoint(CORPUS_URL + PIECE_URL, done)
  });
  it('should return correct image reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.images[0].image).to.be.equal('sources/images/bwv847/file')
    }).end(done)
  });
  it('should return correct image positions reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.images[0].positions).to.be.equal('sources/images/bwv847/positions')
    }).end(done)
  });
  it('should return correct audio file reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.audios[0].file).to.be.equal('sources/audios/bwv847/file')
    }).end(done)
  });
  it('should return correct audio synchro file reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.audios[0]["onset-date"]).to.be.equal('sources/audios/bwv847/synchro')
    }).end(done)
  });
  it('should return correct audio image file reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.audios[0].images[0].image).to.be.equal('sources/audios/bwv847/images/bwv847-waves/file')
    }).end(done)
  });
  it('should return correct audio image positions reference', done => {
    request(app)
    .get(CORPUS_URL + PIECE_URL)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(response => {
      let sources = JSON.parse(response.text).sources;
      expect(sources.audios[0].images[0].positions).to.be.equal('sources/audios/bwv847/images/bwv847-waves/positions')
    }).end(done)
  });
  it('should return piece infos to the admin user', done => {
    checkAuthEndPoint(CORPUS_URL + PIECE_URL, ADMIN_JWT, done)
  });
  it('should return piece infos to an authenticated user', done => {
    checkAuthEndPoint(CORPUS_URL + PIECE_URL, ALGOMUS_MEMBER_JWT, done)
  });
  it('should return piece infos in a sub corpus', done => {
    checkEndPoint(SUB_CORPUS_PIECE_URL, done)
  });
  it('should return piece infos to the admin user (sub corpus)', done => {
    checkAuthEndPoint(SUB_CORPUS_PIECE_URL, ADMIN_JWT, done)
  });
  it('should return piece infos to an authenticated user (sub corpus)', done => {
    checkAuthEndPoint(SUB_CORPUS_PIECE_URL, ALGOMUS_MEMBER_JWT, done)
  });

});

describe('GET ANALYSES', () => {
  it('should return 404 if piece does not exist', done => {
    check404(CORPUS_URL + '/corpus/unknown/analyses', done)
  });
  it('should return piece analyses', done => {
    checkEndPoint(CORPUS_URL + PIECE_URL + '/analyses', done)
  });
  it('should return a piece analyse', done => {
    checkEndPoint(CORPUS_URL + PIECE_URL + '/analyses/computed.dez', done)
  });
  it('should return 404 if the piece analyse does not exist', done => {
    check404(CORPUS_URL + PIECE_URL + '/analyses/unknown.dez', done)
  });
  it('should return piece analyses in a sub corpus', done => {
    checkEndPoint(SUB_CORPUS_PIECE_URL + '/analyses', done)
  });
  it('should return a piece analyse in a sub corpus', done => {
    checkEndPoint(SUB_CORPUS_PIECE_ANALYSIS_URL, done)
  });
});

describe('GET PIECES SOURCES (with sources folder)', () => {
  it('should return 404 if piece does not exist when requesting image positions', done => {
    check404(CORPUS_URL + '/unknown/sources/images/unknown/positions', done)
  });
  it('should return 404 if image does not exist when requesting image positions', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/images/unknown/positions', done)
  });
  it('should return piece image positions', done => {
    checkEndPoint(CORPUS_URL + PIECE_IMAGE_URL + "/positions", done)
  });
  it('should return 404 if piece does not exist when requesting image file', done => {
    check404(CORPUS_URL + '/unknown/sources/images/unknown/file', done)
  });
  it('should return 404 if image does not exist when requesting image file', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/images/unknown/file', done)
  });
  it('should return piece image file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
  it('should return 404 if piece does not exist when requesting audio file', done => {
    check404(CORPUS_URL + '/unknown/sources/audios/unknown/file', done)
  });
  it('should return 404 if audio does not exist when requesting audio file', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/audios/unknown/file', done)
  });
  it('should return piece audio file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_AUDIO_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "audio/mpeg")
    .expect(200, done)
  });
  it('should return 404 if piece does not exist when requesting audio synchro', done => {
    check404(CORPUS_URL + '/unknown/sources/audios/unknown/synchro', done)
  });
  it('should return 404 if audio does not exist when requesting audio synchro', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/audios/unknown/synchro', done)
  });
  it('should return piece audio synchro', done => {
    checkEndPoint(CORPUS_URL + PIECE_AUDIO_URL + '/synchro', done)
  });
  it('should return 404 if piece does not exist when requesting audio image positions', done => {
    check404(CORPUS_URL + '/unknown/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio does not exist when requesting audio image positions', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image positions', done => {
    check404(CORPUS_URL + PIECE_AUDIO_URL + '/images/unknown/positions', done)
  });
  it('should return piece audio image positions', done => {
    checkEndPoint('/corpus/bwv847/sources/audios/bwv847/images/bwv847-waves/positions', done)
  });
  it('should return 404 if piece does not exist when requesting audio image file', done => {
    check404(CORPUS_URL + '/unknown/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio does not exist when requesting audio image file', done => {
    check404(CORPUS_URL + PIECE_URL + '/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image file', done => {
    check404(CORPUS_URL + PIECE_AUDIO_URL + '/images/unknown/file', done)
  });
  it('should return piece audio image file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_AUDIO_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
});

describe('GET PIECES SOURCES (flat files)', () => {
  // it('should return 404 if image does not exist when requesting image positions', done => {
  //   check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/images/unknown/positions', done)
  // });
  it('should return piece image positions', done => {
    checkEndPoint(CORPUS_URL + PIECE_FLAT_IMAGE_URL + '/positions', done)
  });
  it('should return 404 if image does not exist when requesting image file', done => {
    check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/images/unknown/file', done)
  });
  it('should return piece image file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_FLAT_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
  it('should return 404 if audio does not exist when requesting audio file', done => {
    check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/file', done)
  });
  it('should return piece audio file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "audio/mpeg")
    .expect(200, done)
  });
  // it('should return 404 if audio does not exist when requesting audio synchro', done => {
  //   check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/synchro', done)
  // });
  it('should return piece audio synchro', done => {
    checkEndPoint(CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/synchro', done)
  });
  it('should return 404 if audio does not exist when requesting audio image positions', done => {
    check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image positions', done => {
    check404(CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/images/unknown/positions', done)
  });
  it('should return piece audio image positions', done => {
    checkEndPoint(CORPUS_URL + PIECE_FLAT_AUDIO_IMAGE_URL + '/positions', done)
  });
  it('should return 404 if audio does not exist when requesting audio image file', done => {
    check404(CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image file', done => {
    check404(CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/images/unknown/file', done)
  });
  it('should return piece audio image file', done => {
    request(app)
    .get(CORPUS_URL + PIECE_FLAT_AUDIO_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
});

describe('GET PIECES SOURCES (with sources folder) - SUB CORPUS', () => {
  it('should return 404 if piece does not exist when requesting image positions', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/images/unknown/positions', done)
  });
  it('should return 404 if image does not exist when requesting image positions', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/images/unknown/positions', done)
  });
  it('should return piece image positions', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_IMAGE_URL + "/positions", done)
  });
  it('should return 404 if piece does not exist when requesting image file', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/images/unknown/file', done)
  });
  it('should return 404 if image does not exist when requesting image file', done => {
    check404('/corpus/bach/fugues/bwv847/sources/images/unknown/file', done)
  });
  it('should return 404 if image does not exist when requesting image file', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/images/unknown/file', done)
  });
  it('should return piece image file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
  it('should return 404 if piece does not exist when requesting audio file', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/audios/unknown/file', done)
  });
  it('should return 404 if audio does not exist when requesting audio file', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/audios/unknown/file', done)
  });
  it('should return piece audio file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_AUDIO_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "audio/mpeg")
    .expect(200, done)
  });
  it('should return 404 if piece does not exist when requesting audio synchro', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/audios/unknown/synchro', done)
  });
  it('should return 404 if audio does not exist when requesting audio synchro', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/audios/unknown/synchro', done)
  });
  it('should return piece audio synchro', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_AUDIO_URL + '/synchro', done)
  });
  it('should return 404 if piece does not exist when requesting audio image positions', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio does not exist when requesting audio image positions', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image positions', done => {
    check404(SUB_CORPUS_URL + PIECE_AUDIO_URL + '/images/unknown/positions', done)
  });
  it('should return piece audio image positions', done => {
    checkEndPoint('/corpus/bwv847/sources/audios/bwv847/images/bwv847-waves/positions', done)
  });
  it('should return 404 if piece does not exist when requesting audio image file', done => {
    check404(SUB_CORPUS_URL + '/unknown/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio does not exist when requesting audio image file', done => {
    check404(SUB_CORPUS_URL + PIECE_URL + '/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image file', done => {
    check404(SUB_CORPUS_URL + PIECE_AUDIO_URL + '/images/unknown/file', done)
  });
  it('should return piece audio image file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_AUDIO_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
});

describe('GET PIECES SOURCES (flat files) - SUB CORPUS', () => {
  // it('should return 404 if image does not exist when requesting image positions', done => {
  //   check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/images/unknown/positions', done)
  // });
  it('should return piece image positions', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_FLAT_IMAGE_URL + '/positions', done)
  });
  it('should return 404 if image does not exist when requesting image file', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/images/unknown/file', done)
  });
  it('should return piece image file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_FLAT_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
  it('should return 404 if audio does not exist when requesting audio file', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/file', done)
  });
  it('should return piece audio file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "audio/mpeg")
    .expect(200, done)
  });
  // it('should return 404 if audio does not exist when requesting audio synchro', done => {
  //   check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/synchro', done)
  // });
  it('should return piece audio synchro', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/synchro', done)
  });
  it('should return 404 if audio does not exist when requesting audio image positions', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/images/unknown/positions', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image positions', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/images/unknown/positions', done)
  });
  it('should return piece audio image positions', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_IMAGE_URL + '/positions', done)
  });
  it('should return 404 if audio does not exist when requesting audio image file', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_URL + '/sources/audios/unknown/images/unknown/file', done)
  });
  it('should return 404 if audio iameg does not exist when requesting audio image file', done => {
    check404(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_URL + '/images/unknown/file', done)
  });
  it('should return piece audio image file', done => {
    request(app)
    .get(SUB_CORPUS_URL + PIECE_FLAT_AUDIO_IMAGE_URL + '/file')
    .set('Accept', 'application/json')
    .expect('Content-Type', "image/png")
    .expect(200, done)
  });
});

describe('POST ANALYSE', () => {
  it('should forbid if name contains reference.dez', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({name: 'reference.dez'})
    .set('Accept', 'application/json')
    .expect(401, done)
  });
  it('should forbid if name contains computed.dez', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({name: 'computed.dez'})
    .set('Accept', 'application/json')
    .expect(401, done)
  });
  it('should forbid if name is algomus.dez', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({name: 'algomus.dez'})
    .set('Accept', 'application/json')
    .expect(401, done)
  });
  it('should forbid if name is empty.dez', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({name: 'empty.dez'})
    .set('Accept', 'application/json')
    .expect(401, done)
  });
  it('should create an analysis', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({
      name: 'test.dez',
      content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
    })
    .set('Accept', 'application/json')
    .expect(200, done)
  });
  it('should return the new created analyse', done => {
    checkEndPoint(CORPUS_URL + PIECE_URL + '/analyses/test.dez', done)
  });
  it('should create an analysis in a sub corpus', done => {
    request(app)
    .post(SUB_CORPUS_URL + PIECE_URL + '/analyses')
    .send({
      name: 'test.dez',
      content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
    })
    .set('Accept', 'application/json')
    .expect(200, done)
  });
  it('should return the new created analyse in sub corpus', done => {
    checkEndPoint(SUB_CORPUS_URL + PIECE_URL + '/analyses/test.dez', done)
  });
  it('should update an analysis', done => {
    request(app)
    .post(CORPUS_URL + PIECE_URL + '/analyses')
    .send({
      name: 'test.dez',
      content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
    })
    .set('Accept', 'application/json')
    .expect(200, done)
  });
  it('should update an analysis in a sub corpus', done => {
    request(app)
    .post(SUB_CORPUS_URL + PIECE_URL + '/analyses')
    .send({
      name: 'test.dez',
      content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
    })
    .set('Accept', 'application/json')
    .expect(200, done)
  });
});

describe('GROUP PROTECTION', () => {

  describe('GET PROTECTED CORPUS', () => {
    it('should return a 401 on a protected corpus', done => {
      request(app)
      .get(PROTECTED_CORPUS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected corpus to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_CORPUS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected corpus to the admin user', done => {
      checkAuthEndPoint(PROTECTED_CORPUS_URL, ADMIN_JWT, done)
    });
    it('should return a protected corpus to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_CORPUS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PIECE PROTECTED BY CORPUS', () => {
    it('should return a 401 on a protected piece', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_PIECE_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected piece to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_PIECE_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected piece to the admin user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_PIECE_URL, ADMIN_JWT, done)
    });
    it('should return a protected piece to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_PIECE_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED PIECE', () => {
    it('should return a 401 on a protected piece', done => {
      request(app)
      .get(PROTECTED_PIECE_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected piece to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_PIECE_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected piece to the admin user', done => {
      checkAuthEndPoint(PROTECTED_PIECE_URL, ADMIN_JWT, done)
    });
    it('should return a protected piece to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_PIECE_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST BY CORPUS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses', ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses', ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST BY PIECE', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_PIECE_URL + '/analyses', ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_PIECE_URL + '/analyses', ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST', () => {
    it('should return the list without protected analyses names', done => {
      request(app)
      .get(PROTECTED_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .expect(200)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return the list without protected analyses names to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_ANALYSES_LIST_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_ANALYSES_LIST_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET ANALYSIS PROTECTED BY CORPUS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_BY_CORPUS_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_ANALYSIS_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_BY_CORPUS_ANALYSIS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET ANALYSIS PROTECTED BY PIECE', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_BY_PIECE_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_BY_PIECE_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_BY_PIECE_ANALYSIS_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_BY_PIECE_ANALYSIS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSIS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME, ALGOMUS_MEMBER_JWT, done)
    });
  });

})

describe('USER PROTECTION', () => {

  describe('GET PROTECTED CORPUS', () => {
    it('should return a 401 on a protected corpus', done => {
      request(app)
      .get(PROTECTED_USER_CORPUS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected corpus to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_CORPUS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected corpus to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_CORPUS_URL, ADMIN_JWT, done)
    });
    it('should return a protected corpus to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_CORPUS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PIECE PROTECTED BY CORPUS', () => {
    it('should return a 401 on a protected piece', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_PIECE_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected piece to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_PIECE_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected piece to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_PIECE_URL, ADMIN_JWT, done)
    });
    it('should return a protected piece to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_PIECE_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED PIECE', () => {
    it('should return a 401 on a protected piece', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected piece to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected piece to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL, ADMIN_JWT, done)
    });
    it('should return a protected piece to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST BY CORPUS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses', ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses', ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST BY PIECE', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL + '/analyses', ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL + '/analyses', ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST', () => {
    it('should return the list without protected analyses names', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .expect(200)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return the list without protected analyses names to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST BY PIECE', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_PIECE_URL + '/analyses')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL + '/analyses', ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_PIECE_URL + '/analyses', ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSES LIST', () => {
    it('should return the list without protected analyses names', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .expect(200)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return the list without protected analyses names to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(response => {
        let analyses = JSON.parse(response.text);
        expect(analyses).to.not.include(PROTECTED_ANALYSIS_NAME)
      }).end(done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET ANALYSIS PROTECTED BY CORPUS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_BY_CORPUS_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_ANALYSIS_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_ANALYSIS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET ANALYSIS PROTECTED BY PIECE', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_BY_PIECE_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_BY_PIECE_ANALYSIS_URL)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_PIECE_ANALYSIS_URL, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_BY_PIECE_ANALYSIS_URL, ALGOMUS_MEMBER_JWT, done)
    });
  });

  describe('GET PROTECTED ANALYSIS', () => {
    it('should return a 401 on a protected analysis', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME)
      .set('Accept', 'application/json')
      .expect(401, done)
    });
    it('should return a 401 on a protected analysis to an unauthorized user', done => {
      request(app)
      .get(PROTECTED_USER_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
      .expect(401, done)
    });
    it('should return a protected analysis to the admin user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME, ADMIN_JWT, done)
    });
    it('should return a protected analysis to an authorized user', done => {
      checkAuthEndPoint(PROTECTED_USER_ANALYSES_LIST_URL + '/' + PROTECTED_ANALYSIS_NAME, ALGOMUS_MEMBER_JWT, done)
    });
  });

})

describe('PROTECTED POST ANALYSIS', () => {

  describe('GROUP', () => {

    describe('CORPUS LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user creates an analysis', done => {
        request(app)
        .post(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user creates an analysis', done => {
        request(app)
        .post(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should create an analysis', done => {
        request(app)
        .post(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
      it('should return the new created analyse', done => {
        checkAuthEndPoint(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses/test.dez', ALGOMUS_MEMBER_JWT, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

    describe('PIECE LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user creates an analysis', done => {
        request(app)
        .post(PROTECTED_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user creates an analysis', done => {
        request(app)
        .post(PROTECTED_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should create an analysis', done => {
        request(app)
        .post(PROTECTED_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
      it('should return the new created analyse', done => {
        checkAuthEndPoint(PROTECTED_PIECE_URL + '/analyses/test.dez', ALGOMUS_MEMBER_JWT, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

    describe('ANALYSIS LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user updates an analysis', done => {
        request(app)
        .post(PROTECTED_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user updates an analysis', done => {
        request(app)
        .post(PROTECTED_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

  })

  describe('USER', () => {

    describe('CORPUS LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user creates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user creates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should create an analysis', done => {
        request(app)
        .post(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
      it('should return the new created analyse', done => {
        checkAuthEndPoint(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses/test.dez', ALGOMUS_MEMBER_JWT, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_USER_BY_CORPUS_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

    describe('PIECE LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user creates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user creates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should create an analysis', done => {
        request(app)
        .post(PROTECTED_USER_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
      it('should return the new created analyse', done => {
        checkAuthEndPoint(PROTECTED_USER_PIECE_URL + '/analyses/test.dez', ALGOMUS_MEMBER_JWT, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_USER_PIECE_URL + '/analyses')
        .send({
          name: 'test.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

    describe('ANALYSIS LEVEL PROTECTION', () => {
      it('should return 401 when unauthenticated user updates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .expect(401, done)
      });
      it('should return 401 when unauthorized user updates an analysis', done => {
        request(app)
        .post(PROTECTED_USER_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + BAD_GROUP_JWT)
        .expect(401, done)
      });
      it('should update an analysis', done => {
        request(app)
        .post(PROTECTED_USER_ANALYSES_LIST_URL)
        .send({
          name: 'testprotected.dez',
          content: '{"labels": [{"start":8.5,"duration":7.5,"type":"S","tag":"S","staff":"1"}]}'
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + ALGOMUS_MEMBER_JWT)
        .expect(200, done)
      });
    })

  })

})

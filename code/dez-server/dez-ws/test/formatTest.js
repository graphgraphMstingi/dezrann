const assert = require('assert');
const PieceDataFormater = require('../lib/PieceDataFormater');
const AjvJsonFormatValidator = require('../lib/AjvJsonFormatValidator');

let validator = new AjvJsonFormatValidator()
let pieceDataFormater = new PieceDataFormater(validator)

describe('CHECK INFO.JSON FORMAT', () => {
  let input
  beforeEach(() => {
    input = {
      id : "id",
      composer : "composer",
      title : "title",
      sources : {
        images : [ { image : "image", type : "type" } ],
        audios : [
          {
            file : "file",
            "onset-date" : "onset-date",
            images : [ { image : "image", type : "type" } ]
          },
        ]
      }
    }
  });
  it('should throw EMPTY_PIECE_DATA if input is undefined', () => {
    assert.throws(() => { pieceDataFormater.formate(undefined)}, /EMPTY_PIECE_DATA/)
  });
  it('should complain if input is an empty dict', () => {
    assert.throws(() => {pieceDataFormater.formate({})}, /BAD_PIECE_FORMAT/)
  });
  it('should throw BAD_PIECE_FORMAT if input is an empty array', () => {
    assert.throws(() => {pieceDataFormater.formate([])}, /BAD_PIECE_FORMAT/)
  });
  it('should complain if input does not have id key', () => {
    delete input.id
    input.noid = "noid"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if input does not have composer key', () => {
    delete input.composer
    input.nocomposer = "nocomposer"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if input does not have title key', () => {
    delete input.title
    input.notitle = "notitle"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if input does not have sources key', () => {
    delete input.sources
    input.nosources = "nosources"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if  sources images exists but is empty', () => {
    input.sources = { images : [] }
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first image of sources does not have image key', () => {
    delete input.sources.images[0].image
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first image of sources does not have type key', () => {
    delete input.sources.images[0].type
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  // it('should throw an error if input sources audios is empty', () => {
  //   input.sources = { audios : [] }
  //   assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  // });
  it('should complain if first audio of sources does not have file key', () => {
    delete input.sources.audios[0].file
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first audio of sources does not have onset-date key', () => {
    delete input.sources.audios[0]["onset-date"]
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first audio of sources does not have images key', () => {
    delete input.sources.audios[0].images
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if any of sources audios miss a required key', () => {
    input.sources.audios[1] = { file : "file"}
    assert.throws(() => { pieceDataFormater.formate(input)}, /BAD_PIECE_FORMAT/)
    input.sources.audios[1] = { file : "file", "onset-date" : "onset-date"}
    assert.throws(() => { pieceDataFormater.formate(input)}, /BAD_PIECE_FORMAT/)
    input.sources.audios[1] = { "onset-date" : "onset-date"}
    assert.throws(() => { pieceDataFormater.formate(input)}, /BAD_PIECE_FORMAT/)
    input.sources.audios[1] = { "onset-date" : "onset-date", images : [] }
    assert.throws(() => { pieceDataFormater.formate(input)}, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first audio images is empty', () => {
    input.sources.audios[0].images = []
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first image of first audio does not have image key', () => {
    delete input.sources.audios[0].images[0].image
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if first image of first audio does not have type key', () => {
    delete input.sources.audios[0].images[0].type
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is .png', () => {
    input.sources.images[0].image = ".png"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is .jpg', () => {
    input.sources.images[0].image = ".jpg"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is .ext', () => {
    input.sources.images[0].image = ".ext"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is image-name', () => {
    input.sources.images[0].image = "image-name"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is image-name.', () => {
    input.sources.images[0].image = "image-name."
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is image-name.ext', () => {
    input.sources.images[0].image = "image-name.ext"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
  it('should complain if the first image file name is path/to/image.png', () => {
    input.sources.images[0].image = "path/to/image.png"
    assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
  });
})

describe('TEST INFO.JSON FORMATTING', () => {
  let imageFilePath = "sources/images/image-name/file"
  let input
  beforeEach(() => {
    input = {
      id : "id",
      composer : "composer",
      title : "title",
      sources : {
        images : [ { image : "image-name.png", type : "type" } ]
      }
    }
  })
  describe('-> image', () => {
    it('should return the same image file path if it is already well formated', () => {
      input.sources.images[0].image = imageFilePath
      assert.equal(pieceDataFormater.formate(input).sources.images[0].image, imageFilePath)
    });
    it('should return sources/images/image-name/file if the first image file name is image-name.png', () => {
      assert.equal(pieceDataFormater.formate(input).sources.images[0].image, imageFilePath)
    });
    it('should return sources/images/image-name/file if the first image file name is image-name.jpg', () => {
      input.sources.images[0].image = "image-name.jpg"
      assert.equal(pieceDataFormater.formate(input).sources.images[0].image, imageFilePath)
    });
    it('should return sources/images/image2-name/file if the second image file name is image2-name.png', () => {
      input.sources.images[1] = { image : "image2-name.png", type : "type" }
      assert.equal(pieceDataFormater.formate(input).sources.images[1].image, "sources/images/image2-name/file")
    });
    it('should return the same image url', () => {
      let imageUrl = "http://www.example.fr/path/to/image.png"
      input.sources.images[0].image = imageUrl
      assert.equal(pieceDataFormater.formate(input).sources.images[0].image, imageUrl)
    });
  })
  describe('-> image positions', () => {
    it('should return the same file path if it is already well formated', () => {
      let imagePositionsFilePath = "sources/images/image-name/positions"
      input.sources.images[0].positions = imagePositionsFilePath
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, imagePositionsFilePath)
    });
    it('should return the same url', () => {
      let imagePositionsUrl = "http://www.example.fr/path/to/positions.json"
      input.sources.images[0].positions = imagePositionsUrl
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, imagePositionsUrl)
    });
    it('should complain if the first image positions file name is path/to/positions.json', () => {
      input.sources.images[0].positions = "path/to/positions.json"
      assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    });
    it('should complain if the first image positions file name positions', () => {
      input.sources.images[0].positions = "positions"
      assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    });
    it('should complain if the first image positions file name positions.', () => {
      input.sources.images[0].positions = "positions."
      assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    });
    it('should complain if the first image positions file name positions.ext', () => {
      input.sources.images[0].positions = "positions.ext"
      assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    });
    it('should complain if the first image positions file name .json', () => {
      input.sources.images[0].positions = ".json"
      assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    });
    it('should forge file path if positions is not defined for first image', () => {
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, "sources/images/image-name/positions")
    });
    it('should forge file path if positions is not defined for second image', () => {
      input.sources.images[1] = { image : "image2-name.png", type : "type" }
      assert.equal(pieceDataFormater.formate(input).sources.images[1].positions, "sources/images/image2-name/positions")
    });
    it('should return sources/images/image-name/positions if file name is positions.json', () => {
      input.sources.images[0].positions = "positions.json"
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, "sources/images/image-name/positions")
    });
    it('should return sources/images/image-name/positions if file name is image-name-pos.json', () => {
      input.sources.images[0].positions = "image-name-pos.json"
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, "sources/images/image-name/positions")
    });
    it('should return sources/images/image-name/positions if file name is image-name.json', () => {
      input.sources.images[0].positions = "image-name.json"
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, "sources/images/image-name/positions")
    });
    it('should return sources/images/image-name/positions if file name is positions-image-name.json', () => {
      input.sources.images[0].positions = "positions-image-name.json"
      assert.equal(pieceDataFormater.formate(input).sources.images[0].positions, "sources/images/image-name/positions")
    });
  })
  describe('-> audios', () => {
    beforeEach(() => {
        input.sources.audios = [
          {
            file : "audio-name.mp3",
            "onset-date" : "synchro.json",
            images : [ { image : "image-name.png", type : "type" } ]
          },
          {
            file : "audio2-name.mp3",
            "onset-date" : "synchro.json",
            images : [ { image : "image-name2.png", type : "type" } ]
          },
        ]
      })
    describe('-> audio file', () => {
      it('should return sources/audios/audio-name/file if first audio file name is audio-name.mp3', () => {
        assert.equal(pieceDataFormater.formate(input).sources.audios[0].file, "sources/audios/audio-name/file")
      });
      it('should return sources/audios/audio2-name/file if second audio file name is audio2-name.mp3', () => {
        assert.equal(pieceDataFormater.formate(input).sources.audios[1].file, "sources/audios/audio2-name/file")
      });
      it('should return the same url', () => {
        let audioFileUrl = "http://www.example.fr/path/to/file.mp3"
        input.sources.audios = [ { file : audioFileUrl, "onset-date" : "synchro.json" } ]
        assert.equal(pieceDataFormater.formate(input).sources.audios[0].file, audioFileUrl)
      });
      it('should complain if audio file name is badly formated', () => {
        input.sources.audios = [ { file : "file", "onset-date" : "synchro.json" } ]
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
        input.sources.audios = [ { file : "file.ext", "onset-date" : "synchro.json" } ]
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
        input.sources.audios = [ { file : ".mp3", "onset-date" : "synchro.json" } ]
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
        input.sources.audios = [ { file : ".ext", "onset-date" : "synchro.json" } ]
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
      });
    })
    describe('-> audio synchro file', () => {
      it('should return sources/audios/audio-name/synchro if first audio synchro file name is synchro.json', () => {
        assert.equal(pieceDataFormater.formate(input).sources.audios[0]["onset-date"], "sources/audios/audio-name/synchro")
      });
      it('should return sources/audios/audio2-name/synchro if second audio synchro file name is synchro.json', () => {
        assert.equal(pieceDataFormater.formate(input).sources.audios[1]["onset-date"], "sources/audios/audio2-name/synchro")
      });
      it('should return the same url', () => {
        let audioFileUrl = "http://www.example.fr/path/to/file.mp3"
        let audioSynchroFileUrl = "http://www.example.fr/path/to/synchro.json"
        input.sources.audios[0].file = audioFileUrl
        input.sources.audios[0]["onset-date"] = audioSynchroFileUrl
        assert.equal(pieceDataFormater.formate(input).sources.audios[0]["onset-date"], audioSynchroFileUrl)
      });
      it('should complain if audio synchro file name is badly formated', () => {
        input.sources.audios[0]["onset-date"] = "synchro"
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
        input.sources.audios[0]["onset-date"] = "synchro.ext"
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
        input.sources.audios[0]["onset-date"] = ".json"
        assert.throws(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
      });
    })
    describe('-> audio images', () => {
      const AUDIO_IMAGE_FILE_PATH = "sources/audios/audio-name/images/image-name/file"
      beforeEach (() => {
      })
      it('should return the same image file path if it is already well formated', () => {
        input.sources.audios[0].images[0] = { image : AUDIO_IMAGE_FILE_PATH, type : "type" }
        assert.equal(pieceDataFormater.formate(input).sources.audios[0].images[0].image, AUDIO_IMAGE_FILE_PATH)
      });
      it('should return sources/audio-name/images/image-name/file if the first audio image file name is image-name.png', () => {
        assert.equal(pieceDataFormater.formate(input).sources.audios[0].images[0].image, AUDIO_IMAGE_FILE_PATH)
      });
    })
  })
  describe('-> sources', () => {
    it('should be ok if sources.images is undefined as sources.audios is defined', () => {
      delete input.sources.images
      input.sources = {
        audios : [
          {
            file : "audio-name.mp3",
            "onset-date" : "synchro.json",
            images : [ { image : "image-name.png", type : "type" } ]
          }
        ]
      }
      assert.doesNotThrow(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    })
    it('should be ok if sources.images is defined but sources.audios is undefined', () => {
      assert.doesNotThrow(() => { pieceDataFormater.formate(input) }, /BAD_PIECE_FORMAT/)
    })
  })
});

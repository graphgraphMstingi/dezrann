let assert = require('assert');
let iotl = require('./ioTestLib.js')

describe("Collab server", () => {

  it ('create test', (done) => {

    async function execute (asserts) {
      await iotl.initialisation()
      let client = new iotl.Client(1)
      await client.connect()
      client.create(new iotl.Label ({id : "label", duration : 10}))

      client.on("accept_channel", () => client.emit('analysis-demande'))

      client.on('analysis-channel', (data) => {
        client.disconnect()
        asserts(data)
      })

    }

    execute((data) => {
      assert.equal(data.current_version, 1)
      let a = data.current_analysis
      assert.equal(a[a.length-1].id, "label")
      done()
    })

  })

  it ('create test with some resizing', (done) => {

    async function execute (asserts) {
      await iotl.initialisation()
      let client = new iotl.Client(1)
      await client.connect()
      client.create(new iotl.Label ({id : "label", duration : 10}))

      let ok1 = 0
      client.on("accept_channel", () => {
        ok1++
        if (ok1 == 5) {
          client.emit('analysis-demande')
        }
      })

      client.on('analysis-channel', (data) => {
        client.disconnect()
        asserts(data)
      })

      await client.multiResizesLabel(10, 2, "op1", "change1")

    }

    execute((data) => {
      assert.equal(data.current_version, 11)
      let a = data.current_analysis
      assert.equal(a.length, 1)
      let lastLabel = a[a.length-1]
      assert.equal(lastLabel.id, "label")
      assert.equal(lastLabel.duration, 30)
      done()
    })

  })

  it ('2 clients move 2 labels', (done) => {

    const NB_MVT1 = 10
    const NB_MVT2 = 15

    async function execute (asserts) {
      await iotl.initialisation([
        new iotl.Label ({id : "label1"}),
        new iotl.Label ({id : "label2", start : 20})
      ])
      let client1 = new iotl.Client(2)
      await client1.connect()
      await client1.setLabel("label1")
      let client2 = new iotl.Client(2)
      await client2.connect()
      await client2.setLabel("label2")

      let ok1 = 0
      let ok2 = 0
      client1.on("accept_channel", () => {
        ok1++
        if (ok1 == NB_MVT1 + 1 && ok2 == NB_MVT2 + 1) {
          client2.emit('analysis-demande', {})
        }
      })
      client2.on("accept_channel", () => {
        ok2++
        if (ok1 == NB_MVT1 && ok2 == NB_MVT2) {
          client2.emit('analysis-demande', {})
        }
      })

      client2.on('analysis-channel', (data) => {
        client1.disconnect()
        client2.disconnect()
        asserts(data)
      })

      await iotl.clientsCurrentlyMoveLabels (client1, client2, NB_MVT1, NB_MVT2)
    }

    execute((data) => {
      assert.equal(data.current_version, NB_MVT1 + NB_MVT2 + 2)
      let a = data.current_analysis
      assert.equal(a.length, 2)
      a.forEach(l => {
        if (l.id == "label1") {
          assert.equal(l.start, 2*NB_MVT1)
        } else { // label
          assert.equal(l.start, 20 + 2*NB_MVT2)
        }
      })
      done()
    })
  })

  it ('a client move a label and another change the type of the same label', (done) => {

    const NB_MVT1 = 10
    const NB_MVT2 = 1

    async function execute (asserts) {
      await iotl.initialisation([new iotl.Label ({id : "label1"})])
      let client1 = new iotl.Client(1)
      await client1.connect()
      await client1.setLabel("label1")
      let client2 = new iotl.Client(1)
      await client2.connect()
      await client2.setLabel("label1")

      let ok1 = 0
      let ok2 = 0
      client1.on("accept_channel", () => {
        ok1++
        if (ok1 == NB_MVT1 && ok2 == NB_MVT2) {
          client2.emit('analysis-demande', {})
        }
      })
      client2.on("accept_channel", () => {
        ok2++
        if (ok1 == NB_MVT1 && ok2 == NB_MVT2) {
          client2.emit('analysis-demande', {})
        }
      })

      client2.on('analysis-channel', (data) => {
        client1.disconnect()
        client2.disconnect()
        asserts(data)
      })

      let nb_mvt1 = 0
      let nb_mvt2 = 0
      while (nb_mvt1 < NB_MVT1 || nb_mvt2 < NB_MVT2) {
        let turn = Math.floor(Math.random() * 2)
        if (turn == 0 && nb_mvt1 < NB_MVT1) {
          await client1.moves(2, "op1" + nb_mvt1, "change1" + nb_mvt1)
          nb_mvt1++
        } else if (turn == 1 && nb_mvt2 < NB_MVT2){
          await client2.changeType("Subject", "op2" + nb_mvt1, "change2" + nb_mvt1)
          nb_mvt2++
        }
      }
    }

    execute((data) => {
      assert.equal(data.current_version, NB_MVT1 + NB_MVT2 + 1)
      let a = data.current_analysis
      assert.equal(a.length, 1)
      assert.equal(a[0].start, 2*NB_MVT1)
      assert.equal(a[0].type, "Subject")
      done()
    })
  })

  it ('2 clients move the same label at the same time', (done) => {

    const NB_MVT_INIT = 5
    const NB_MVT1 = 10
    const NB_MVT2 = 15

    async function execute (asserts) {
      await iotl.initialisation([new iotl.Label ({id : "label1"})])
      let client1 = new iotl.Client(1)
      await client1.connect()
      await client1.setLabel("label1")
      let client2 = new iotl.Client(1)
      await client2.connect()
      await client2.setLabel("label1")

      let ok1 = 0
      let ko2 = 0
      client1.on("accept_channel", () => {
        ok1++
        if (ok1 == NB_MVT1 + NB_MVT_INIT && ko2 == NB_MVT2) {
          client2.emit('analysis-demande', {})
        }
      })
      client2.on("deny_channel", () => {
        ko2++
        if (ok1 == NB_MVT1 + NB_MVT_INIT && ko2 == NB_MVT2) {
          client2.emit('analysis-demande', {})
        }
      })

      client2.on('analysis-channel', (data) => {
        client1.disconnect()
        client2.disconnect()
        asserts(data)
      })

      await client1.multiMovesLabel(NB_MVT_INIT, 2, "op1", "change1")
      await iotl.clientsCurrentlyMoveLabels (client1, client2, NB_MVT1, NB_MVT2)
    }

    execute((data) => {
      assert.equal(data.current_version, NB_MVT1 + NB_MVT_INIT + 1)
      let a = data.current_analysis
      assert.equal(a.length, 1)
      assert.equal(a[0].start, 2*(NB_MVT1+NB_MVT_INIT))
      done()
    })
  })

})

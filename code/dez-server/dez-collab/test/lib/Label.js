class Label {
  constructor (data = {}) {
    this._id = data.id || this._randomId()
    this._type = data.type || "Pattern"
    this._tag = data.tag || ""
    this._start = data.start || 0
    this._duration = data.duration || 5
    this._staff = data.staff || 1
    this._line = data.line || "staves.1"
    this._action = "CREATE"
    this._changes = {
      type : this._type,
      tag : this._tag,
      start : this._start,
      duration : this._duration,
      staff : this._staff,
      line : this._line
    }
  }

  currentOp () {
    let changes = { id : this._randomId() }
    Object.keys(this._changes).forEach(k => changes[k] = this._changes[k])
    return {
      id : this._randomId(),
      // Pourquoi deux _randomId() différents ?
      labelId : this._id,
      action : this._action,
      changes : changes
    }
  }

  _randomId() {
    return (Math.random()*10000000).toString(16).substr(0,4)
    +'-'+(new Date()).getTime()
    +'-'+Math.random().toString().substr(2,5);
  }

  move (dx, direction) {
    this._start = this._start + dx * direction
    this._action = "UPDATE"
    this._changes = { start : this._start<0?0:this._start }
  }

  set line (value) {
    this._line = value
    this._action = "UPDATE"
    this._changes = { line : this._line}
  }

  set type (value) {
    this._type = value
    this._action = "UPDATE"
    this._changes = { type : this._type }
  }

  set tag (value) {
    this._tag = value
    this._action = "UPDATE"
    this._changes = { tag : this._tag }
  }

  set comment (value) {
    this._comment = value
    this._action = "UPDATE"
    this._changes = { comment : this._comment }
  }

  setAttribute (name, value) {
    this._action = "UPDATE"
    this._changes = {}
    this._changes[name] = value
  }

}

module.exports = Label

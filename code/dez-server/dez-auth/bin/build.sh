#!/bin/bash

# cd `pwd`;
BUILD_DIR="./build";

if [[ -d $BUILD_DIR ]]
then
    rm -Rf $BUILD_DIR
fi
mkdir $BUILD_DIR;

cp -Rf *.js package* $BUILD_DIR;

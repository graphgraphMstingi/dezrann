
#!env python3
# coding: utf8

# Problèmes à résoudre encore:
# - Savoir dans quelle voix est une note ou un silence:
#   la position ne suffit pas car je ne sais pas  partir de quelle position une
#   note est passe d'une voix à l'autre
#   Solution graphique: connaitre les limites extrèmes d'une staffs. J'ai bien
#   peur que 2 staffs puissent se chevaucher...
#   Solution precise: recuperer la staff a partir d'une note. Champ?
#   get_parent()?...
# - les donnees generees sont consequence et leur balyage en javascript est a
#   optimiser
#   Existe-t-il des bibliothèque JS qui prennent en entreé une image et son
#   mapping structurel en JSON par exemple
#   Une optimisation efficace serai de ne stoquer pour un même offset, les
#   position différente par voix:
#
# Remarques:
# - par défaut 'lilypnd --png fichier.ly' génére un png en 101 dpi (?!)


import re
import subprocess
import os
import os.path
import tools

try:
    import compress
except:
    pass

# output floats with 2 digits
from json import encoder
encoder.FLOAT_REPR = lambda x: format(x, '.2f')


LILYPOND_HEADER = '''
\include "ly/staff-infos.ly"
\include "ly/bar-pos.ly"
\include "ly/notes-pos.ly"

\paper {
   page-breaking = #ly:one-line-breaking
   left-margin   = 46\mm
   right-margin  = 46\mm
   paper-height = 200\mm
}

\header {
   tagline = ##f 
   composer = ##f
}

\layout
{
   \context {
    \Score
    barNumberVisibility = #(every-nth-bar-number-visible 5)
    \override BarNumber.break-visibility = #end-of-line-invisible
    \override BarNumber.self-alignment-X = #CENTER
  }
}
'''

# from ly2video
GLOBAL_STAFF_SIZE = 20

def staffSpacesToPixels(ss, dpi):
    staffSpacePoints = GLOBAL_STAFF_SIZE / 4
    points = ss * staffSpacePoints
    # Donald Knuth's TeX points
    pointsPerInch = 72.27
    inches = points / pointsPerInch
    return inches * dpi


def noteInVoice(note, voice):
    return note['bottom'] >= voice['top'] and note['top'] <= voice['bottom']

DPI = 110
JS_PX_FACTOR = 2
JS_PX_TRANSLATE = 28.0
JS_PX_FACTOR_X = 0.8
JS_PX_TRANSLATE_X = 180.0
STAFF_MARGIN = 20  # peut-être à gérer dans le JS
LEFT_BAR_X = 200.0
VOICES_MARGIN = 1 # between two voices on the same staff


def parseStaffPos(lines, infos):
    voices = []
    staffpos = ""
    for line in lines:
        m = re.match('^->\ *-(\d+\.\d+)\ *-(\d+\.\d+)$', line)
        if not m:
            continue
        notepos = m.groups()
        top = (
            staffSpacesToPixels(float(notepos[0]), DPI) * JS_PX_FACTOR
            + JS_PX_TRANSLATE - STAFF_MARGIN
        )  # * (0.79)
        bottom = (
            staffSpacesToPixels(float(notepos[1]), DPI) * JS_PX_FACTOR
            + JS_PX_TRANSLATE + STAFF_MARGIN
        )  # * (1.15)

        if 'voices-per-staff' in infos and infos['voices-per-staff'] == 2:
            voice = {}
            voice['top'] = top
            voice['bottom'] = (top + bottom) / 2 - VOICES_MARGIN / 2
            voice['notespos'] = {}
            voices.append(voice)

            voice = {}
            voice['top'] = (top + bottom) / 2 + VOICES_MARGIN / 2
            voice['bottom'] = bottom
            voice['notespos'] = {}
            voices.append(voice)
        else:
            voice = {}
            voice['top'] = top
            voice['bottom'] = bottom
            # voice['notes']  = []
            voice['notespos'] = {}
            voices.append(voice)

        staffpos += "voice: (" +\
                    str(voice['top']) + ", " +\
                    str(voice['bottom']) + ")\n"


    # print(staffpos)
    return voices

def parseBars(lines):
    SIGNATURE_X = 80.0
    NOTES_START_X = LEFT_BAR_X + SIGNATURE_X
    measures = [NOTES_START_X]
    for line in lines:
        m = re.match('^\[measure-bar-pos\]\ *(\d+\.\d+)\ *(\d+\.\d+)\ *(\d+\.\d+)$', line)
        if not m:
            continue
        barpos = m.groups()
        averagepos = (float(barpos[0]) + float(barpos[1]))/2
        pos = staffSpacesToPixels(averagepos, DPI) + LEFT_BAR_X
        if (pos not in measures):
            measures.append(pos)

    measures.sort()
    return measures

def parseNotes(lines):
    error = ""
    notes = {}
    for line in lines:
        m = re.match('^\[note-or-rest-pos\]\ *(\d+\.\d+)\ *(\d+\.\d+)\ *(\d+\.\d+)$', line)
        if not m:
            continue
        notepos = m.groups()
        left = float(notepos[0])
        right = float(notepos[1])
        x = (right + left)/2
        onset = float(notepos[2])*4.0
        if onset not in list(notes.keys()):
            notes[onset]={}
            notes[onset]["x"]=x
            notes[onset]["positions"]=[]
        if x != notes[onset]["x"]:
            avg = 0
            for pos in notes[onset]["positions"]:
                avg += (float(pos["left"])+float(pos["right"]))/2
            notes[onset]["x"]=avg/len(notes[onset]["positions"])
            # print("Diff %s %f %f %d" % (onset, x, notes[onset]["x"], len(notes[onset]["positions"])))
        note = {}
        note["left"] = notepos[0]
        note["right"] = notepos[1]
        notes[onset]["positions"].append(note)

    # print(json.dumps(notes, sort_keys=True, indent=4))

    notestab = []
    for key in sorted(notes.keys()):
        note = {}
        note["x"] = staffSpacesToPixels(notes[key]["x"], DPI) + LEFT_BAR_X
        note["onset"] = key
        notestab.append(note)

    return notestab



def prepareLily(f):
    '''
    Adds LILYPOND_HEADER to the provided file
    '''
    f_dez = f.replace('.raw.ly', '.ly')
    print('==> ' + f_dez)
    out = open(f_dez, 'w')
    out.write(LILYPOND_HEADER)
    for l in open(f):
        if 'title' in l or 'lilypond-book-preamble' in l:
            continue

        # Add a StaffGroup gathering all the staves
        if r'\score' in l:
            l += r'\new StaffGroup'

        out.write(l)
    out.close
    return f_dez


def process(outdir, PIECE_INDEX, f, infos, run=True, staffTopMargin=0, staffBottomMargin=0):
    positions_name = "positions.json"
    if not run:
        return positions_name, {}

    f_dez = prepareLily(f)
    cmd = ["lilypond", "--png", "-dresolution=" + str(DPI), f_dez]

    stdout = subprocess.check_output(cmd)


    # Move files to outdir
    os.system('mv -v %s* %s' % (PIECE_INDEX, outdir))

    # Process stdout
    lines = str(stdout).split(r'\n')
    
    score = {}
    score["onset-x"] = parseNotes(lines)
    score["bars"] = parseBars(lines)
    score["staffs"] = parseStaffPos(lines, infos)

    # Write 'positions.json'
    tools.write_json(score, outdir + "/" + positions_name)

    # Compress '.png' and '.json'
    try:
        compress.compress_piece(outdir + '/%s.png' % PIECE_INDEX, staffTopMargin, staffBottomMargin)
    except Exception as e:
        print('! Error in compression', e)

    return positions_name, score

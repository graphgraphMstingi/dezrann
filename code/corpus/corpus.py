#!env python3

## This file is part of Dezrann <http://www.dezrann.net>
## Copyright (C) 2016-2021 by Algomus Team at CRIStAL (UMR CNRS 9189, Université Lille),
## in collaboration with MIS (UPJV, Amiens) and LITIS (Université de Rouen).
##
## Dezrann is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Dezrann is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Dezrann.  If not, see <https://www.gnu.org/licenses/>.


import music21
import os
import os.path
import sys
import shutil
import piece
import glob
import music

import argparse

parser = argparse.ArgumentParser(description = 'Prepare corpora for the Dezrann server. See add-to-dezrann.txt.')
parser.add_argument('--available', '-a', action='store_true', default=False, help='show available corpora')

parser.add_argument('--limit', '-l', type=int, default=0, help='limit to the first n pieces')
parser.add_argument('--filter', '-f', type=str, default='', help='filter on the title of the piece')

parser.add_argument('--no-lilypond', '-L', type=str, default='', help='do not run lilypond')
parser.add_argument('--musicxml2ly', '-m', action='store_true', default='False', help='use musicxml2ly command (bypass music21)')
parser.add_argument('--staff-top-margin', '-t', type=int, default=0, help='top margin for each staff')
parser.add_argument('--staff-bottom-margin', '-b', type=int, default=0, help='bottom margin for each staff')

parser.add_argument('corpora', nargs='*', help='''corpora''')



MUSIC21_CORPUS = '../../../music21/music21/corpus/'

replaces_fr = {
    'major': 'majeur', 'minor': 'mineur',
    'C': 'do', 'D': 'ré', 'E': 'mi', 'F': 'fa', 'G': 'sol', 'A': 'la', 'B': 'si'
}

def tonality_fr(t):
    for (k, v) in replaces_fr.items():
        t = t.replace(k, v)
    return t

class Corpus():

    PATH_OUT = './data/'

    def __init__(self):
        raise NotImplementedError("Corpus() should be subclassed")

    def prepare(self, score, infos):
        return score

    def process(self, limit=0, filter='', run_lilypond=True, musicxml2ly=False, staffTopMargin=0, staffBottomMargin=0):
        os.system('mkdir -p %s/%s' % (self.PATH_OUT, self.ID))

        processed = 0
        for (file_in, file_out, file_audio, infos) in self.files:

            f = self.PATH_IN + file_in
            f_lily = self.PATH_OUT + self.ID + '/' + file_out
            ff_id = os.path.basename(f_lily).replace('.raw.ly', '')

            if filter:
                if not filter in ff_id:
                    continue

            print()
            ok = piece.process(f, f_lily, ff_id, infos, file_audio, run_lilypond, musicxml2ly, staffTopMargin, staffBottomMargin)
            print()

            if not ok:
                continue

            processed += 1
            if (processed == limit):
                break

        print('==> processed %d files' % processed)

    def processReference(self, limit = 0):
        processed = processed_ok = 0
        for (file_in, file_out) in self.references:
            f_in = os.path.join(self.REF_PATH_IN, file_in)
            f_out = os.path.join(self.REF_PATH_OUT, file_out)
            print('   %s -> %s' % (f_in, f_out))
            try:
                shutil.copy(f_in, f_out)
                processed_ok += 1
            except Exception as e:
                print(e)
                #continue

            processed += 1
            if (processed == limit):
                break

        print('==> copied %d / %d files' % (processed_ok, processed))


    def __str__(self):
        s = '=== %s (%d files)\n' % (self.ID, len(self.files))

        composers = []
        ids = []
        for f, ff, fa, infos in self.files:
            if infos:
                if  'composer' in infos:
                  composers += [infos['composer']]
        s += 'Composer: ' + ' '.join(set(composers)) + '\n'

        if self.PATH_IN:
            s += 'Path: ' + self.PATH_IN + '\n'

        s += ' '.join(sorted(ids))
        return s

####

class GenericCorpus(Corpus):
    '''
    A generic corpus.
    Do not include here any specificity related to a corpus, but subclass if necessary.
    '''

    PATH_IN = ''

    def __init__(self, id, files, ids = {}, name_as_id=False):
        self.ID = id

        self.files = []
        for i, f in enumerate(glob.glob(files)):

            base, ext = os.path.splitext(os.path.basename(f))

            num = ids[base] if base in ids else (base if name_as_id else '%03d' % i)

            id_file = '%s-%s' % (id, num)

            self.files.append((f, id_file + '.raw.ly', None,
                               { 'title': os.path.basename(f),
                                 'composer': id_file
                               }))

    def prepare(self, score, infos):
        print(infos)

        infos['title'] = score.metadata.title
        infos['composer'] = score.metadata.composer
        return score

####


class BachChorales(Corpus):
    PATH_IN = MUSIC21_CORPUS + '/bach/'
    ID = 'bach-chorales'

    def __init__(self):
        bcl = music21.corpus.chorales.ChoraleList()
        self.files = [('bwv%s.mxl' % bwv, 'bwv%s.raw.ly' % bwv, None,
                       {
                           'title': '%s, BWV %s' % (bcl.byBWV[bwv]['title'], bwv),
                           'composer': 'J.-S. Bach',
                           'voices-per-staff': 2
                       }) for bwv in bcl.byBWV]

####

class Omnibook(Corpus):
    PATH_IN = '../../../charlie-parker-omnibook/xml/'
    ID = 'parker-omnibook'

    def __init__(self):

        self.files = [(song+".xml" , song+".raw.ly", None,
                       {
                           'title': title,
                           'composer': 'Charlie Parker'
                       }) for(song, title) in [
                            ("An_Oscar_For_Treadwell", "An Oscar For Treadwell"),
                            ("Another_Hairdo", "Another Hairdo"),
                            ("Anthropology", "Anthropology"),
                            ("Au_Private_1", "Au Private (ver.1)"),
                            ("Au_Private_2", "Au Private (ver.2)"),
                            ("Back_Home_Blues", "Back Home Blues"),
                            ("Barbados", "Barbados"),
                            ("Billie's_Bounce", "Billie's Bounce"),
                            ("Bird_Gets_The_Worm", "Bird Gets The Worm"),
                            ("Bloomdido", "Bloomdido"),
                            ("Blue_Bird", "Blue Bird"),
                            ("Blues_For_Alice", "Blues For Alice"),
                            ("Buzzy", "Buzzy"),
                            ("Card_Board", "Card Board"),
                            ("Celerity", "Celerity"),
                            ("Chasing_The_Bird", "Chasing The Bird"),
                            ("Cheryl", "Cheryl"),
                            ("Chi_Chi", "Chi Chi"),
                            ("Confirmation", "Confirmation"),
                            ("Cosmic_Rays", "Cosmic Rays"),
                            ("Dewey_Square", "Dewey Square"),
                            ("Diverse", "Diverse"),
                            ("Donna_Lee", "Donna Lee"),
                            ("KC_Blues", "KC Blues"),
                            ("Kim_1", "Kim (ver.1)"),
                            ("Kim_2", "Kim (ver.2)"),
                            ("Ko_Ko", "Ko Ko"),
                            ("Laird_Baird", "Laird Baird"),
                            ("Marmaduke", "Marmaduke"),
                            ("Mohawk_1", "Mohawk (ver.1)"),
                            ("Mohawk_2", "Mohawk (ver.2)"),
                            ("Moose_The_Mooche", "Moose The Mooche"),
                            ("My_Little_Suede_Shoes", "My Little Suede Shoes"),
                            ("Now's_The_Time_1", "Now's The Time (ver.1)"),
                            ("Now's_The_Time_2", "Now's The Time (ver.2)"),
                            ("Ornithology", "Ornithology"),
                            ("Passport", "Passport"),
                            ("Perhaps", "Perhaps"),
                            ("Red_Cross", "Red Cross"),
                            ("Relaxing_With_Lee", "Relaxing With Lee"),
                            ("Scrapple_From_The_Apple", "Scrapple From The Apple"),
                            ("Segment", "Segment"),
                            ("Shawnuff", "Shawnuff"),
                            ("Si_Si", "Si Si"),
                            ("Steeplechase", "Steeplechase"),
                            ("The_Bird", "The Bird"),
                            ("Thriving_From_A_Riff", "Thriving From A Riff"),
                            ("Visa", "Visa"),
                            ("Warming_Up_A_Riff", "Warming Up A Riff"),
                            ("Yardbird_Suite", "Yardbird Suite"),
                        ]]
        self.files.sort()
####


class BachFugues(Corpus):
    PATH_IN = '../../../algomus/projets/fugue/data/wtc-i.osu/'
    ID = 'bach-fugues'
    REF_PATH_IN = '../../../algomus/projets/fugue/out/dez/'
    REF_PATH_OUT = '../../corpus/'

    def __init__(self):
        self.files = [('wtc-i-%02d.krn' % num, 'bwv%d.raw.ly' % (845 + num), 'data/bach-fugues/synchro/wtc-i-%02d.mp3' % num,
                       {
                           'title': 'Fuga #%02d in %s, BWV %s' % (num, tonality, 845 + num),
                           'title:fr': 'Fugue n°%02d en %s, BWV %s' % (num, tonality_fr(tonality), 845 + num),
                           'composer': 'J.-S. Bach',
                           'audios.info': 'Recording by Kimiko Ishizaka. See https://www.welltemperedclavier.org/',
                           'images.info': 'Score generated by Lilypond, from a .krn file downloaded from KernScore http://kern.humdrum.org/'
                       }) for (num, tonality) in [
                           (1, 'C major'),
                           (2, 'C minor'),
                           (3, 'C♯ major'), # pb ?
                           (4, 'C♯ minor'), # pb ?
                           (5, 'D major'),
                           (6, 'D minor'),
                           (7, 'E♭ major'),
                           (8, 'D♯ minor'),
                           (9, 'E major'),
                           (10, 'E minor'),
                           (11, 'F major'), # 3/8
                           (12, 'F minor'), # pb ?
                           (13, 'F♯ major'),
                           (14, 'F♯ minor'), # 6/4
                           (15, 'G major'), # pb ?
                           (16, 'G minor'),
                           (17, 'A♭ major'),
                           (18, 'G♯ minor'),
                           (19, 'A major'), # 9/8
                           (20, 'A minor'), # pb ?
                           (21, 'B♭ major'),
                           (22, 'B♭ minor'), # 2/2
                           (23, 'B major'),
                           (24, 'B minor'), # pb ?
                      ]]

        # os.system("cd ../../../algomus/projets/fugue && python3 fugue --dez-export-reference --no-analysis  wtc-i")
        self.references = [('wtc-i-%02d-reference.dez' % num,
                            os.path.join('bwv%d/' % (845 + num), 'analyses/%02d-reference.dez' % num))
                           for num in range(1, len(self.files)+1)]

class MozartQuartets(Corpus):
    PATH_IN = '../../../humdrum-mozart-quartets/kern/'
    # https://github.com/musedata/humdrum-mozart-quartets
    ID = 'mozart-quartets'

    def __init__(self):
        self.files = []
        for mvt in [1, 2, 3, 4]:
            self.files += [('k%03d-%02d.krn' % (k, mvt), 'k%d.%d.raw.ly' % (k, mvt), None,
                       {
                           'title': 'String Quartet #%02d in %s, K %d, mvt. %d' % (num, tonality, k, mvt),
                           'title:fr': 'Quatuor n°%02d en %s, K %d, mvt. %d' % (num, tonality_fr(tonality), k, mvt),
                           'composer': 'W.-A. Mozart',
                       }) for (num, k, tonality) in [
                           (1, 80,  'G major'),
                           (2, 155, 'D major'),
                           (3, 156, 'G major'),
                           (4, 157, 'C major'),
                           (5, 158, 'F major'),
                           (6, 159, 'B♭ major'),
                           #(7, 160, 'E♭ major'),
                           (8, 168, 'F major'),
                           (9, 169, 'A major'),
                           #(10, 170, 'C major'),
                           (11, 171, 'E♭ major'),
                           (12, 172, 'B♭ major'),
                           (13, 173, 'D minor'),
                           # (0, 285, '???'), # Flute
                           # (0, 298, '???'), # Flute
                           # (0, 370, '???'), # Oboe
                           (14, 387, 'G major'),
                           (15, 421, 'D minor'),
                           (16, 428, 'E♭ major'),
                           (17, 458, 'B♭ major'),
                           #(18, 464, 'A major'),
                           (19, 465, 'C major'),
                           (20, 499, 'D major'),
                           #(21, 575, 'D major'),
                           (22, 589, 'B♭ major'),
                           (23, 590, 'F major'),
                      ]]
            self.files.sort()

####

class HaydnQuartets(Corpus):
    PATH_IN = '../../../algomus/corpus/haydn-quartets/'
    ID = 'haydn-quartets'

    def __init__(self):

        self.files = [('Haydn-%02d-%01d-%s.krn' % (op,quat,mvt), 'Haydn.%d.%d.%s.raw.ly' % (op,quat,mvt), None,
            {
               'title': 'String Quartet #%d, Op. %d No. %d, mvt. %s' % (num, op, quat, mvt),
               'title:fr': 'Quatuor n°%d, Op. %d n°%d, mvt. %s' % (num, op, quat, mvt),
               'composer': 'J. Haydn',
            }) for (num,op,quat,mvt) in [
                (1,17,1,"i"),
                (2,17,2,"i"),
                (3,17,3,"iv"),
                (4,17,5,"i"),
                (5,17,6,"i"),
                (6,20,1,"iv"),
                (7,20,3,"iii"),
                (8,20,3,"iv"),
                (9,20,4,"iv"),
                (10,20,5,"i"),
                (11,20,6,"ii"),
                (12,33,1,"i"),
                (13,33,1,"iii"),
                (14,33,2,"i"),
                (15,33,3,"iii"),
                (16,33,4,"i"),
                (17,33,5,"i"),
                (18,33,5,"ii"),
                (19,50,1,"i"),
                (20,50,1,"iv"),
                (21,50,2,"i"),
                (22,50,2,"iv"),
                (23,50,3,"iv"),
                (24,50,4,"i"),
                (25,50,5,"iv"),
                (26,50,6,"i"),
                (27,50,6,"ii"),
                (28,54,1,"i"),
                (29,54,1,"ii"),
                (30,54,2,"i"),
                (31,54,3,"i"),
                (32,54,3,"iv"),
                (33,55,1,"ii"),
                (34,55,2,"ii"),
                (35,55,3,"i"),
                (36,64,3,"i"),
                (37,64,3,"iv"),
                (38,64,4,"i"),
                (39,64,6,"i"),
                (40,71,1,"i"),
                (41,74,1,"i"),
                (42,74,1,"ii")]
        ]

        self.files.sort()

####
class HarmonicSequenceCorpus(Corpus):
    PATH_IN = '../../../algomus/projets/marches/data/'
    ID = 'harmonic-sequences'

    def __init__(self):
        self.files = []

        self.files += [('sonata05-1.krn', 'sonata05-1.raw.ly', None,
        {
           'title': 'Piano Sonata No.5, Op.10 No.1',
           'composer': 'Ludwig van Beethoven'
        })]
        self.files += [('Beethoven-101-28.krn', 'Beethoven-101-28.raw.ly', None,
        {
           'title': 'Piano Sonata No.28, Op.101',
           'composer': 'Ludwig van Beethoven'
        })]
        self.files += [('wtc-ii-18.krn', 'wtc-ii-18.raw.ly', None,
        {
           'title': 'Prelude and Fugue in G-sharp minor, BWV 887',
           'composer': 'J.-S. Bach'
        })]
        self.files += [('bwv0565.krn', 'bwv0565.raw.ly', None,
        {
           'title': 'Toccata and Fugue in D minor, BWV 565',
           'composer': 'J.-S. Bach'
        })]
        # print(self.files)
        self.files.sort()


class BeethovenSonatas(Corpus):
    PATH_IN = 'data/beethoven-sonatas/'
    ID = 'beethoven-sonatas'

    def __init__(self):

        self.files = [('beethoven_sonata%02d_mov1.krn' % (num), 'Beethoven.%d.1.raw.ly' % (num), None,
            {
               'title': 'Sonata #%d, mvt. 1' % (num),
               'title:fr': 'Sonates n°%d, mvt. 1' % (num),
               'composer': 'L. v. Beethoven',
            }) for (num) in range (1, 32)
        ]

        self.files.sort()


class MendelssohnQuartets(Corpus):
    PATH_IN = 'data/mendelssohn/'
    ID = 'mendelssohn-quartets'

    def __init__(self):
        self.files = [(
            'mend1_op12_1.xml',
            'mend1_op12_1.raw.ly',
            None,
            {
                'title': 'Quartet Mendelssohn',
                'composer': 'Félix Mendelssohn'
            }
        )]


class TheSession(Corpus):
    PATH_IN = 'data/thesession/files/'
    ID = 'the-session'

    def __init__(self):
        with open('data/thesession/corpus.json') as json_file:
            import json
            tunes = json.load(json_file)
            self.files = []
            for tune in tunes :
                self.files.append((
                    tune['file'],
                    tune['file'].split('.')[0] + ".raw.ly",
                    None,
                    {
                        "composer" : tune["composer"],
                        "title" : tune["title"]
                    }
                ))
####


CORPORA = [
    GenericCorpus('fantasias', '../../../algomus/projets/fantasia/data/*.krn', name_as_id = True),
    GenericCorpus('sbg', '../../../algomus/projets/sonata/data/sbg/*.xml'),

    BachFugues(),
    BachChorales(),

    MozartQuartets(),
    HaydnQuartets(),
    MendelssohnQuartets(),

    BeethovenSonatas(),

    HarmonicSequenceCorpus(),

    Omnibook()
    TheSession()
]

CORPORA_DICT = { corpus.ID: corpus for corpus in CORPORA }


####

if __name__ == '__main__':
    args = parser.parse_args()

    if args.available:
        for corpus in CORPORA:
            print(corpus)

    for c in args.corpora:
        try:
            corpus = CORPORA_DICT[c]
        except KeyError:
            print("## Corpus '%s' not found" % c)
            continue

        print(corpus)
        corpus.process(filter=args.filter, limit=args.limit, run_lilypond=not args.no_lilypond,
                       musicxml2ly=(args.musicxml2ly=='True'), staffTopMargin=args.staff_top_margin, staffBottomMargin=args.staff_bottom_margin)



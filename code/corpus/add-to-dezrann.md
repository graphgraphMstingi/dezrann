


# Procédure 2021 pour ajouter un fichier

http://dezrann.net/add-new-piece/


# Procédure 2021 pour ajouter un corpus

### Prérequis
- python3
- music21 (c'est le music21 actuel, pas le notre), matplotlib, numpy, scipy et pydub :
 `pip3 install matplotlib numpy scipy pydub`
- lilypond

- les .krn/.mid/.mxl ou autres doivent être accessibles sur un git externe à Dezrann (type `../../../humdrum-mozart-quartets/kern/`)


### Ajouter le corpus dans `/code/corpus/corpus.py`

- Idéalement, faire une nouvelle sous-classe comme MozartQuartets()
  Travailler les méta-données et la liste des pièces pour avoir un beau référencement,
  type tonalité, numéro d'oeuvre/de quatuor, en rajoutant ces infos à la main (voir dans MozartQuartets())
  Et aussi de jolis identifiants type bwv847.

- Une autre option, découragée, est d'utiliser GenericCorpus
  (et identifiants corpus-001 ou corpus-nom-du-fichier)

- Rajouter cela dans `CORPORA`

- Lancer `python3 corpus.py mon-corpus`. Possible de faire `-l 3` pour avoir juste les trois premières pièces

- Ouvrir *tous* les `.png` générés par lilypond et vérifier que "tout va bien",
  en particulier en regardant à la dernière mesure s'il n'y a pas eu de gros décalage rythmique.

- Si certaines pièces ne passent pas, les commenter dans la liste (et/ou retravailler les .krn ou autres)


### Quand c'est prêt

- Commiter vos modifications sur `corpus.py` sur une nouvelle branche
  (`git checkout -b corpus-blabla`), pousser, faire une merge request (MR).
  Assigner cette MR à Mathieu, qui regarde, lance, demande éventuellement des modifs 
  et donne à Manu ou déploie en prod un `.tar.gz` quand c'est fini.


### Détails

- on peut rajouter `"hide": true` dans le `info.json` si on veut cacher ces pièces
    (voir `stand-by-me/info.json` dans `prod-internal`)






# Ajout d'un fichier (ancienne procédure)

Pour ajouter un seul fichier, on peut ne pas modifier corpus.py et directement appeler d'autres scripts,
puis remettre les /corpus/* dans le git... mais ce n'est pas très reproductible par la suite.
À ne faire que quand on fait des tests sur des nouvelles vues / générations (type stand-by-me).

=======

Pour ajouter un .mp3 edmus (sur branche edmus)

  - créer un répertoire /code/corpus/data/edmus/id-piece/, mettre id-piece.mp3 et d'autres choses sources, committer l'ensemble

  - depuis code/corpus, lancer
      python3 piece.py data/edmus/id-piece/id-piece.mp3
    => création et remplissage de /corpus/id-piece/...
  - dans synchro.json *et* id-piece-waves-pos.json, remplacer le 99 de "date" par la durée en secondes (moins quelques secondes ?) #644

  - si besoin, éditer aussi les positions des staves pour que ce soit joli ?

  - si lyrics : mettre id-piece.lyrics, et appeler
      python3 lyrics.py id-piece
  - puis éditer info.json à la main pour rajouter ce qu'il faut
  (voir stand-by-me/info.json)  #645. En particulier la propriété `hide` à
  positionner à `true` dans le cas d'edmus.

  - committer le répertoire résultant (y compris les .png et .mp3...)

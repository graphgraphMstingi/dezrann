#!/usr/bin/python3
import requests, json
import os
from slugify import slugify

THE_SESSION_DIR = "data/thesession/"
THE_SESSION_FILES_DIR = THE_SESSION_DIR + "files/"
THE_SESSION_ENDPOINT = "https://thesession.vercel.app/thesession/tunes.json?_sort=rowid&tune_id__exact="

def ensurePathExists(path):
    if (not os.path.exists(path)):
        os.makedirs(path)

class SelectedTuneData :

    def __init__(self, line):
        splittedLine = line.replace("\n", "").rstrip().split(':')
        self.name = splittedLine[0]
        self.id = splittedLine[1].split(',')[0]
        self.version = 1
        if len(splittedLine) == 3:
            self.version = int(splittedLine[2])

class TheSessionServer :

    def __init__(self, endPoint) :
        self._endPoint =  endPoint
        
    def fetchTune (self, id) :
        url = self._endPoint + id
        print("  -> fetch tune " + url)
        r = requests.get(url)
        return json.loads(r.text)

def articleFirst (title) :
    article = ""
    splittedTitle = title.split(", The")
    if len(splittedTitle) == 2 :
        article = "The "
    return article + splittedTitle[0]

def makePieceInfos (tune, filename) :
    return {
        "title": articleFirst(tune.name) + " (" + tune.type + ")",
        "composer": "Trad. (The Session " + tune.id + "-" + str(tune.version) + ")",
        "file": filename,
        "time-signature": tune.meter
    }

def saveCorpusIndexFile (corpus) :
    corpusFile = open(THE_SESSION_DIR + "corpus.json", "w")
    corpusFile.write(json.dumps(corpus, sort_keys=True, indent=4))
    corpusFile.close()

def toDict (values, keys) :
    ret = {}
    for i in range(len(keys)):
        ret[keys[i]] = str(values[i])
    return ret


def toABC(tune):
    return "T: " + tune.name + "\n"\
        + "R: " + tune.type + "\n"\
        + "M: " + tune.meter + "\n"\
        + "K: " + tune.mode + "\n"\
        + tune.abc

class Tune :

    def __init__(self, data, version):
        dict = toDict(data["rows"][version-1], data["columns"])
        self.name = dict["name"]
        self.id = dict["tune_id"]
        self.username = dict["username"]
        self.meter = dict["meter"]
        self.type = dict["type"]
        self.mode = dict["mode"]
        self.abc = dict["abc"]
        self.version = version
    
class SelectedTunes (list) :

    def __init__(self, filePath) :
        selectedTunesFile = open(filePath, "r")
        for line in selectedTunesFile:
            self.append(SelectedTuneData(line))
        selectedTunesFile.close()

def withoutArticle (tuneName) :
    return tuneName.split(", The")[0]

class ABCFile :

    def __init__(self, tuneId, tuneName) :
        self.name = tuneId + "-" + slugify(withoutArticle(tuneName)) + ".abc"
    
def createABCFile (tune, directory) :
    abcFile = ABCFile(tune.id, tune.name)
    ensurePathExists(directory)
    f = open(directory + abcFile.name, "w")
    f.write(toABC(tune))
    f.close()
    return abcFile

if __name__ == '__main__':
    theSessionServer = TheSessionServer(THE_SESSION_ENDPOINT)
    corpusInfos = []
    for selectedTune in SelectedTunes(THE_SESSION_DIR + "selectedTunes"):
        print(selectedTune.name)
        tune = Tune(theSessionServer.fetchTune(selectedTune.id), selectedTune.version)
        abcFile = createABCFile(tune, THE_SESSION_FILES_DIR)
        corpusInfos.append(makePieceInfos(tune, abcFile.name))
    saveCorpusIndexFile (corpusInfos)
    print(json.dumps(corpusInfos, sort_keys=True, indent=4))

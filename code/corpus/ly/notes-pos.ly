#(define (grob-get-ancestor-with-interface grob interface axis)
   (
      let ((parent (ly:grob-parent grob axis))) (
         if (null? parent) #f (
            if (grob::has-interface parent interface) parent (
               grob-get-ancestor-with-interface parent interface axis
            )
         )
       )
   )
)

#(define (grob-get-paper-column grob)
   (grob-get-ancestor-with-interface grob 'paper-column-interface X)
)


#(define (print-rest-infos-debug grob)
   (
      let* (
         (extent       (ly:grob-extent grob grob X))
         (system       (ly:grob-system grob))
         (x-extent     (ly:grob-extent grob system X))
         (left         (car x-extent))
         (right        (cdr x-extent))
         (y-extent     (ly:grob-extent grob system Y))
	       (bottom       (car y-extent))
	       (top          (cdr y-extent))
         (paper-column (grob-get-paper-column grob))
         (time         (ly:grob-property paper-column 'when 0))
      )
      (
         if (not (equal? (ly:grob-property grob 'transparent) #t)) (
            format #t "\nNote ~10,2f ~10,2f ~10,2f ~10,2f  ~10,4f ~a"
                   (* (/ (* top 5.0) 72.27) 110)
                   (* (/ (* bottom 5.0) 72.27) 110)
                   left
                   right
                   (+ 0.0 (ly:moment-main time) (* (ly:moment-grace time) (/ 9 40)))
                   system
         )
       )
   )
)

#(define (print-notes-infos grob)
   (
      let* (
         (extent       (ly:grob-extent grob grob X))
         (system       (ly:grob-system grob))
         (x-extent     (ly:grob-extent grob system X))
         (left         (car x-extent))
         (right        (cdr x-extent))
         (paper-column (grob-get-paper-column grob))
         (time         (ly:grob-property paper-column 'when 0))
      )
      (
         if (not (equal? (ly:grob-property grob 'transparent) #t)) (
            format #t "\n[note-or-rest-pos] ~23,16f ~23,16f ~23,16f"
                   left
                   right
                   (+ 0.0 (ly:moment-main time) (* (ly:moment-grace time) (/ 9 40)))
         )
       )
   )
)

\layout {
  \context {
    \Voice
    \override NoteHead  #'after-line-breaking = #print-notes-infos
    \override Rest  #'after-line-breaking = #print-notes-infos
  }
}

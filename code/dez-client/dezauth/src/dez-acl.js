/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/communication-icons.js';

class DezAcl extends PolymerElement {
  static get template() {
    return html`
      <style>
        table {
          margin-top: 10px;
        }
        td {
          padding-right: 20px;
        }
      </style>

      <table>
        <template is="dom-repeat" items="[[acl]]" as="access" mutable-data>
          <tr>
            <td><iron-icon icon="communication:vpn-key"></iron-icon></td>
            <td>[[access.verb]]</td>
            <td>
              <template is="dom-repeat" items="[[access.subjects]]" as="subject" mutable-data>
                [[subject]]
              </template>
            </td>
          </tr>
        </template>
      </table>

    `;
  }

  static get properties() {
    return {
      acl: {
        type: Array,
        value: []
      }
    }
  }

}

window.customElements.define('dez-acl', DezAcl);

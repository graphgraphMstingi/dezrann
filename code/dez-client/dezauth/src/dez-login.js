/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-meta/iron-meta.js';
import './dez-global-variable.js';
import './dez-logout.js';
import './shared-styles.js';

class DezLogin extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        .wrapper-btns {
          margin-top: 15px;
        }
        paper-button.link {
            color: #757575;
        }
        .alert-error {
          background: #ffcdd2;
          border: 1px solid #f44336;
          border-radius: 3px;
          color: #333;
          font-size: 14px;
          padding: 10px;
        }
      </style>

      <iron-meta id="config" key="config" value="{{config}}"></iron-meta>

      <div class="card">
        <div id="unauthenticated" hidden$="[[storedUser.loggedin]]">
          <h1>Log In</h1>
          <template is="dom-if" if="[[error]]">
            <p class="alert-error"><strong>Error:</strong> [[error]]</p>
          </template>
          <paper-input label="Username" value="{{formData.uid}}"></paper-input>
          <paper-input label="Password" type="password" value="{{formData.password}}"></paper-input>
          <div class="wrapper-btns">
            <paper-button raised class="primary" on-tap="postLogin">Log In</paper-button>
          </div>

          <p>A personal account enables to save your analyses in a private space.
          <br/><a href="mailto:dezrann@algomus.fr?Subject=Dezrann&Body=%0ADear%20Algomus%20team%2C%0A%0AI%20would%20like%20to%20see%20the%20following%20pieces%20in%20Dezran%20:%20...%0A%0AI%20would%20like%20to%20have%20an%20account%20to%20store%20analyses%20for%20my%20research/teaching/...%0A%0AI%20have%20the%20following%20suggestion%20/%20bug%20report:%20...%0A%0APlease%20keep%20me%20informed%20of%20the%20development%20of%20Dezrann.%0A%0ABest%20regards%2C%0A%0A">Request an account</a>
          </p>
        </div>
        <div id="authenticated" hidden$="[[!storedUser.loggedin]]">
          <h2>Hello, [[storedUser.name]]!</h2>
          <p>You are currently logged in. You can access <a href="[[rootPath]]corpus">Dezrann ws</a>!</p>
          <dez-logout stored-user="{{storedUser}}"></dez-logout>
        </div>
      </div>

      <iron-ajax
          id="loginAjax"
          method="post"
          content-type="application/json"
          handle-as="text"
          on-response="handleUserResponse"
          on-error="handleUserError">
      </iron-ajax>

      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <dez-global-variable key="userData" value="{{storedUser}}"></dez-global-variable>

      <app-location route="{{route}}"></app-location>

    `;
  }

  static get properties() {
    return {
      formData: {
        type: Object,
        value: {}
      },
      storedUser: Object,
      error: String
    }
  }

  postLogin() {
    this.$.loginAjax.url = JSON.parse(this.config).dezauth + "/authenticate";
    this.$.loginAjax.body = this.formData;
    this.$.loginAjax.generateRequest();
  }

  handleUserResponse(event) {
    var response = JSON.parse(event.detail.response);

    if (response.access_token) {
      this.error = '';
      this.storedUser = {
        name: this.formData.uid,
        access_token: response.access_token,
        loggedin: true
      };
      this.set('route.path', '/corpus');
    }

    // reset form data
    this.formData = {};
  }

  handleUserError(event) {
    this.error = event.detail.request.xhr.response;
  }

}

window.customElements.define('dez-login', DezLogin);

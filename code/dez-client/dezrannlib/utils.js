async function responseOf(ajaxComponent) {
    let request = ajaxComponent.generateRequest();
    try {
        await request.completes;
        return request.parseResponse();
    } catch (err) {
        console.log(err);
    }
}

async function sleep (ms) {
    return new Promise (resolve => setTimeout(() => resolve(), ms))
}

class ShapeFactory {

    constructor (view) {
        this._view = view
    }

    set shapeDefaults (values) {
        this._shapeDefaults = values;
    }

    // create Shapes
    create (type) {
        if (type == 'Rectangle') {
            return new Rectangle(
                this._view,
                new ResizeDecoratorsCollection(this._view),
                this._shapeDefaults && this._shapeDefaults['Rectangle']
            );
        } else if (type == 'VerticalLine') {
            return new VerticalLine(
                this._view,
                new ResizeDecoratorsCollection(this._view),
                this._shapeDefaults && this._shapeDefaults['VerticalLine']
            );
        } else if (type == 'Triangle') {
            return new Triangle(
                this._view,
                undefined,
                this._shapeDefaults && this._shapeDefaults['Triangle']
            );
        }
    }

}

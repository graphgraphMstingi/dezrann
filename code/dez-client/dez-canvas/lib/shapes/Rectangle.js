class Rectangle extends Shape {

    constructor (view, resizers, defaults = {}) {
        super(view, resizers, defaults)
        if (defaults && defaults["text"] != undefined) {
            this._DEFAULT_TEXT = defaults["text"];
        } else {
            this._DEFAULT_TEXT = "rectangle"
        }
        this._HORIZONTAL_MARGIN = 10;
        this._MARGIN_WIDTH = 10
        this._MARGIN_HEIGHT = 20
        this._resizers = resizers
        this._selected = false;

        this._canBeOnSeveralLines = defaults.canBeOnSeveralLines;
        this._canBeOnAllLines = defaults.canBeOnAllLines;
    }

    _setFlag (flag, defaultValue) {
        if (flag == undefined) {
            flag = defaultValue;
        }
        return flag;
    }

    get canBeOnSeveralLines () {
        return this._setFlag(this._canBeOnSeveralLines, true);
    }

    get canBeOnAllLines () {
        return this._setFlag(this._canBeOnAllLines, false);
    }

    show (x, y, width, height) {
        let the_x = x - this._HORIZONTAL_MARGIN;
        let the_width = width + 2*this._HORIZONTAL_MARGIN - 2;
        if (this._rect == undefined) {
            this._rect = this._view.createRectangle(
                the_x, y, the_width, height, this._color, this._OPACITY
            )
        } else {
            this._rect.redraw(the_x, y, the_width, height, this._color)
        }
        if (this._text == undefined) {
            this.text = this._DEFAULT_TEXT
        }
        this.$placeText(the_x, y, the_width, height)
        if (this._selected) {
          this.$showAsSelected()
        }
    }

    /**
    * protected
    */
    $placeText(x, y, width, height) {
      this._text.width = this._rect.width
      this._text.moveTo(x + this._MARGIN_WIDTH, y + this._MARGIN_HEIGHT)
    }

    remove () {
        super.remove()
        if (this._rect != undefined) {
            this._rect.remove();
            this._resizers.deleteAll();
        }
    }

    showAsSelected () {
        this._selected = true;
        this.$showAsSelected()
    }

    /**
    * protected
    */
    $showAsSelected () {
      // this._rect is undefined
      // if the shape has never been shown (this.show())
      if (this._rect != undefined) {
        this._rect.strokeWidth = 2;
        this._displayResizers()
      }
    }

    _displayResizers () {
      if (this._rect.height == 0) {
        this._resizers.deleteAll()
      } else {
        if (this._selected && this._resizers.hidden) {
          this._resizers.decorate(this._rect);
        } else {
          this._resizers.update(this._rect)
        }
      }
    }

    showAsUnselected () {
        this._selected = false;
        this._rect.strokeWidth = 0;
        this._resizers.deleteAll()
    }

    contains (x) {
        if (this._rect == undefined) return false;
        return this._rect.contains(x) || this._resizers.contains(x);
    }

}

class CanvasEvent {

    constructor (event, view) {
        this._event = event;
        this._view = view;
    }

    get x () { return this._view.x(this._event.detail.x) }
    get y () { return this._view.y(this._event.detail.y) }
    get dx () {
        let dx = this._event.detail.dx
        if (dx == undefined) {
            dx = 0
        }
        return this._view.length(dx);
    }

    get dy () {
        let dy = this._event.detail.dy
        if (dy == undefined) {
            dy = 0
        }
        return this._view.length(dy);
    }

    get ox () { return this.x - this.dx }
    get oy () { return this.y - this.dy }
    get state () { return this._event.detail.state; }

    get target () {
        let target = Polymer.dom(this._event).rootTarget;
        if (target.nodeName == "image" || target.nodeName == "svg") {
            return "background"
        } else if (target.classList[0] == "left-resize-decorator") {
            return target.classList[0];
        } else if (target.classList[0] == "right-resize-decorator") {
            return target.classList[0];
        }
        return target.nodeName;
    }

    get targetIsGrob () {
      return (
        this.target == "rect" ||
        this.target == "right-resize-decorator" ||
        this.target == "left-resize-decorator"
      )
    }

}

/**
 * Class representing the main graphical data structure. It is composed by
 * Lines that themselves contain Grobs. Canvas need a view to display Grobs.
 * If no line is defined, a default line that lays on all the Canvas surface is
 * used. This default line is deleted when the first line is added.
 * Each line belongs to a Group.
 */
class Canvas {

    /**
     * The conctructor injects a view as a dependency. The canvas communicates
     * with the view to have the same height. The canvas need to have the
     * correct height to adapt the default line dimensions. The canvas informs
     * the view when a line is added so that the view adapt its height..
     * @param  {Object} view the view of the canvas
     */
    constructor (view, initDefaultGroups) {
        this._view = view;

        this._DEFAULT_LINE_WIDTH = 30;
        this._INTERLINE = 2;

        this._defaultGroup = 'staves'
        this.initOneLine()
        this._globalGrobs = []

        this._updateGroups()
    }

    /**
     * Init the groups
     */
    initOneLine() {
        this._defaultLine = true
        this._groupIds = ['staves']
        let staves = new LinesGroup('staves', this)
        staves.addLine(0, this._view.height)
        this._groups = {'staves': staves}
    }

    initDefaultGroups() {
        this._defaultLine = false
        this._groupIds = ['top', 'staves', 'bot']
        let top = new LinesGroup('top', this)
        top.addRegularLines(3)
        this._view.addSpaceAtTop(top.height);
        let bot = new LinesGroup('bot', this)
        bot.addRegularLines(3)
        this._groups = {
          'top': top,
          'staves': new LinesGroup('staves', this),
          'bot': bot
        }
        this._updateGroups()
    }

    /**
     * Update group indexes and positions while scanning the lines
     */
    _updateGroups() {
        this._groups[this._groupIds[0]].top = 0
        for (let i=1; i<this._groupIds.length; i++) {
            let previousGroup = this._groups[this._groupIds[i-1]]
            this._groups[this._groupIds[i]].top = previousGroup.bottom
        }
        this._view.changeHeightIfNeeded(this._bottomLine.bottom);
    }

    /**
     * If no line have been added, there is a default line that lays on all the
     * canvas surface. This method updates this line bottom if the view height
     * has changed. It can be changed if the background image has been loaded
     * after the call to the Canvas constructor.
     * @todo it should be more logical that is the view that informs the canvas
     * when the image is loaded...
     * @private
     */
    _checkDefaultLineHeight () {
        if (this._defaultLine && this._height != this._view.height) {
            this._groups['staves'].lineBottom.bottom = this._view.height
        }
    }

    /**
     * @property
     * @private
     * @return {Number} current height of the canvas
     */
    get _height () {
      return this._groups[this._groupIds[this._groupIds.length-1]].lineBottom.bottom
    }

    /**
     * Add a component to the canvas.
     * If it is a Grob, it returns 'this' and another method has to be chained.
     * The methods are on(), onLineIndex() or from().
     * @public
     * @param {Object} component a Grob
     * @return {Canvas} this if component is a Grob
     */
    add (grob) {
        this._componentToAdd = grob;
        return this
    }

    updateOrAddLines (lines, groupId) {
        if (this._defaultLine) {
            this.initDefaultGroups()
        }
        if (typeof groupId === 'undefined') {
            groupId = this._defaultGroup
        }
        let group = this._groups[groupId]

        lines.sort((l1,l2) => l1.top - l2.top)

        if (lines.length > group.linesNb) {
            // Add new lines
            for (let i=0; i<lines.length; i++) {
              group.addLine(lines[i]['top'], lines[i]['bottom'])
            }
        } else {
            // Update existing lines
            group.updateLines(lines)
        }
        this._updateGroups()
    }

    /**
     * Has to be chained to the add() method:
     *
     *      canvas.add(grob).on(y)
     *
     * It adds the component on the line
     * that contains y. If the default line is used we correct its height if
     * needed.
     * @public
     * @param  {Number} y vertical position
     */
    on (y) {
        if (y == undefined) {
            throw "Can't add grob because y is undefined"
        }
        this._checkDefaultLineHeight()
        let the_group = Object.values(this._groups).filter(group => group.on(y))[0]
        if (the_group) {
          the_group.addGrobOn(this._componentToAdd, y)
        } else {
          throw "No group here!"
        }
    }

    /**
     * Has to be chained to the add() method:
     *
     *      canvas.add(grob).onLineIndex(index))
     *
     * It adds the component on the line that has the given index in the
     * this._lines array. If the default line is used we correct its height if
     * needed.
     * @todo it is used to load data from json format. A loadJson() method might
     * be better so that the index is not handle from outside a Canvas instance.
     * @public
     * @param  {Number} index [description]
     */
    indexFromId (id) {
        let group = id.split('.')[0];
        let lineIndex = parseInt(id.split('.')[1]);

        return this._groups[group].lineTop + lineIndex - 1 ;
    }

    onLineId (id) {
        let groupId = id.split('.')[0]
        let lineIndex = parseInt(id.split('.')[1])
        this._checkDefaultLineHeight()
        this._groups[groupId].getLine(lineIndex).add(this._componentToAdd)
    }

    onAllLines () {
        this._checkDefaultLineHeight()
        this._globalGrobs.push(this._componentToAdd)
        Object.values(this._groups).forEach(group => group.addGrobOnAllLines(this._componentToAdd));
    }

    putGrobOnAllLines(grob) {
      Object.values(this._groups).forEach(group => group.addGrobOnAllLines(grob));
    }

    /**
     * Grobs are deleted by GrobFactory and Canvas. Should be deleted by only
     * one!
     * TO REFACTOR
     */
    deleteGlobal (id) {
        this._globalGrobs = this._globalGrobs.filter(grob => grob.id != id);
    }

    /**
     * Grobs are deleted by GrobFactory and Canvas. Should be deleted by only
     * one!
     * TO REFACTOR
     */
    deleteGlobals () {
        this._globalGrobs = []
    }

    /**
     * Has to be chained to the add() method and the toY() method has to be
     * chained. It stores the starting vertical position in this._fromY.
     * @param  {Number} y the starting vertical position
     * @return {Canvas}   return this
     */
    from (y) {
        this._fromY = y
        return this
    }

    /**
     * @private
     * @return {Line} the highest line
     */
    get _topLine () {
      return this._groups[this._groupIds[0]].lineTop
    }

    /**
     * @private
     * @return {Line} the lowest line
     */
    get _bottomLine () {
      return this._groups[this._groupIds[this._groupIds.length-1]].lineBottom
    }

    /**
     * @param  {Number} from starting vertical position
     * @param  {Number} to   ending vertical position
     * @return {Boolean}     true if there are lines between starting and ending
     * vertical positions, false otherwise.
     */
    _insideLines (from, to) {
        return (from > this._topLine.top && to < this._bottomLine.bottom)
            || (to > this._topLine.top && from < this._bottomLine.bottom)
    }

    _nbLinesBetween(from, to) {
      return Object.values(this._groups)
        .reduce((nb, group) => nb + group.nbLinesBetween(from, to), 0)
        // return this._lines.filter(line => line.in(from, to)).length;
    }

    /**
     * Has to be chained to the from() method:
     *
     *      canvas.add(grob).from(y1).toY(y2)
     *
     * It adds the grob into the lines between this._fromY and y.
     * @public
     * @param  {Number} y the ending vertical position
     */
    toY (y) {
        if (!this._insideLines(this._fromY, y)) return;
        let grob = this._componentToAdd;
        if (grob.shape.canBeOnSeveralLines) {
            if (this._nbLinesBetween(this._fromY, y) >= 2
                    && grob.shape.canBeOnAllLines) {
                this.add(grob).onAllLines();
            } else {
                grob.leavesFromAllParents()
                Object.values(this._groups)
                  .filter(group => group.in(this._fromY, y))
                  .forEach(group => group.addGrobFromTo(grob, this._fromY, y))
            }
        } else if (this._nbLinesBetween(this._fromY, y) > 0){
            grob.leavesFromAllParents()
            for (let id of this._groupIds) {
              let group = this._groups[id]
              if (group.in(this._fromY, y)) {
                try {
                  group.addGrobOn(grob, this._fromY)
                } catch (e) {}
              }
            }
        }
    }

    move(grob) {
        this._grobToMove = grob;
        return this
    }

    toLineOn(y) {
      let group = Object.values(this._groups).filter(group => group.on(y))[0]
      if (group) {
        group.moveGrobToLineOn(this._grobToMove, y)
      }
    }

    toLineIdOn(id) {
      this._groups[id.split('.')[0]].moveGrobToLineId(this._grobToMove, id)
    }

    /**
     * Store the highest vertical position to get the Children (Lines) from. The
     * to() method has to be chained with it. It is used by a line to get its
     * 'sisters'.
     * @public
     * @param  {Number} top highest vertical position to get the children
     * (Lines) from.
     * @return {Canvas}     this
     */
    getChildrenFrom (top) {
        this._childrenFromTop = top;
        return this
    }

    /**
     * Has to be chained to the getChildrenFrom() method:
     *
     *      canvas.getChildrenFrom(top).to(bottom)
     *
     * @param  {Number} bottom lowerest vertical position to get the children
     * (Lines) from.
     * @return {Array}  list of children between top and bottom (included).
     */
    to (bottom) {
        let top = this._childrenFromTop;
        return Object.values(this._groups)
          .filter(group => group.in(top, bottom))
          .reduce((linesIn, group) => linesIn.concat(group.linesIn(top, bottom)), [])
    }

    _groupAbove (group) {
      let aboveId = this._groupIds[this._groupIds.indexOf(group.id)-1]
      return this._groups[aboveId]
    }

    _groupBelow (group) {
      let belowId = this._groupIds[this._groupIds.indexOf(group.id)+1]
      return this._groups[belowId]
    }

    _grobOnGroups(above, below, x) {
      for (let grobAbove of above.lineBottom.grobsOn(x)) {
        for (let grobBelow of below.lineTop.grobsOn(x)) {
          if (grobBelow == grobAbove) return grobAbove;
        }
      }
      return undefined
    }

    grobOn (x, y) {
      let group = Object.values(this._groups).filter(group => group.on(y))[0]
      if (group) {
        let grob = group.grobOn(x,y)
        if (grob) {
          return grob
        } else if (group.hasLineBelow(y)) {
          let above = this._groupAbove(group)
          if (above) {
            return this._grobOnGroups(above, group, x)
          }
        } else if (group.hasLineAbove(y)) {
          let below = this._groupBelow(group)
          if (below) {
            return this._grobOnGroups (group, below, x)
          }
        }
      }
      return undefined
    }

    /**
     * Return the line which contains y. If the default line is used we correct
     * its height if needed.
     * @public
     * @param  {Number} y vertical position
     * @return {Line}   the line which contains y
     */
    deleteLines () {
        if (Object.values(this._groups).filter(group => !group.empty).length == 0) {
            this._view.setToMinHeight();
            this.initDefaultGroups()
        } else {
            throw "some lines are not empty";
        }
    }

}

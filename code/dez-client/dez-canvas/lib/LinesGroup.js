

class LinesGroup {

    constructor (id, parent) {
        this.id = id
        this._top = 0         // Top y-position of the group
        this._lines = []
        this._DEFAULT_LINE_WIDTH = 30;
        this._INTERLINE = 2;
        this._parent = parent
    }

    addLine (top, bottom) {
      let line = new Line(top + this.top, bottom + this.top)
      line.parent = this._parent
      this._lines.push(line);
      this._lines.sort((l1,l2) => l1.top - l2.top);
      this._lines.forEach((line, index) => line.id = this.id + "." + (index + 1));
    }

    addRegularLines(nb) {
        let height = 0
        for (let i=0; i<nb; i++) {
            this.addLine(height, height + this._DEFAULT_LINE_WIDTH)
            height += this._DEFAULT_LINE_WIDTH + this._INTERLINE;
        }
    }

    updateLines(lines) {
      for (let i=0; i<lines.length; i++) {
          this._lines[i].top = lines[i]['top'] + this.top
          this._lines[i].bottom = lines[i]['bottom'] + this.top
      }
    }

    on (y) {
      return this.top <= y && y <= this.top + this.height
    }

    in (y1, y2) {
      if (y1 < y2) {
          return this.top <= y2 && this.bottom >= y1
      } else {
          return this.top <= y1 && this.bottom >= y2
      }
    }

    addGrobOn(grob, y) {
      let line = this._lines.filter(line => line.on(y))[0]
      if (line) {
        line.add(grob)
      } else {
        throw 'No line here!'
      }
    }

    addGrobFromTo(grob, from, to) {
      this._lines
        .filter(line => line.in(from, to))
        .forEach(line => line.add(grob))
    }

    addGrobOnAllLines (grob) {
      this._lines.forEach(line => line.add(grob))
    }

    /**
     * @private
     * @param  {Number} y given vertical position
     * @return {Line}   the line above y
     */
    _lineAbove(y) {
        let aboves = this._lines.filter(line => line.bottom < y)
        if (aboves.length == 0) return undefined
        return aboves.reduce(
            (max, line) => line.bottom>max.bottom?line:max,
            aboves[0]
        )
    }

    /**
     * @private
     * @param  {Number} y given vertical position
     * @return {Line}   the line below y
     */
    _lineBelow(y) {
        let belows = this._lines.filter(line => line.top > y)
        if (belows.length == 0) return undefined
        return belows.reduce(
            (min, line) => line.top<min.top?line:min,
            belows[0]
        )
    }

    /**
     * Return the grob that stands on (x,y). If there is no line on y, it
     * returns the grob that stands on lines below and above (x,y).
     * @public
     * @param  {Number} x horizontal position
     * @param  {Number} y vertical position
     * @return {Grob}   the grob on (x,y)
     */
    grobOn (x, y) {
      let line = this._lines.filter(line => line.on(y))[0]
      if (line) {
        let grobs = line.grobsOn(x);
        let selected = grobs.filter(grob => grob.selected);
        return selected.length>0?selected[0]:grobs[0]
      } else { // interline
        let above = this._lineAbove(y);
        if (above == undefined) return undefined;
        let below = this._lineBelow(y);
        if (below == undefined) return undefined;
        for (let grobAbove of above.grobsOn(x)) {
            for (let grobBelow of below.grobsOn(x)) {
                if (grobBelow == grobAbove) return grobAbove;
            }
        }
      }
      return undefined
    }

    hasLineAbove (y) {
      return this._lineAbove(y) != undefined
    }

    hasLineBelow (y) {
      return this._lineBelow(y) != undefined
    }

    _lineOn(y) {
        return this._lines.filter(line => line.on(y))[0]
    }

    moveGrobToLineOn(grob, y) {
      let toLine = this._lineOn(y);
      if (toLine) {
        if (!grob.hasParentOn(y)) {
          if (grob.higherThan(y)) {
            grob.topestParent.delete(grob);
          } else if (grob.lowerThan(y)) {
            grob.bottomestParent.delete(grob);
          }
          toLine.add(grob);
        }
      }
    }

    moveGrobToLineId(grob, id) {
      let toLine = this._lines.filter(line => line.id == id)[0]
      if (toLine) {
        if (grob.higherThan(toLine.top)) {
          grob.topestParent.delete(grob);
        } else if (grob.lowerThan(toLine.bottom)) {
          grob.bottomestParent.delete(grob);
        }
        toLine.add(grob);
      }
    }

    linesIn(from, to) {
      return this._lines.filter(line => line.in(from, to))
    }

    get empty () {
      return this._lines.filter(line => !line.empty).length == 0
    }

    nbLinesBetween (from, to) {
      return this._lines
        .filter(line => line.in(from, to)).length
    }

    getLine (index) {
      return this._lines[index-1]
    }

    get linesNb () {
      return this._lines.length
    }

    get lineTop () {
      return this._lines[0]
    }

    get lineBottom () {
      return this._lines[this.linesNb -1]
    }

    get top () {
      return this._top
    }

    set top (top) {
      let verticalMove = top - this._top
      this._top = top
      this._lines.forEach(line => line.shift(verticalMove))
    }

    get bottom () {
      return this.top + this.height
    }

    get height () {
      return this.linesNb==0?0:this.lineBottom.bottom - this.top + this._INTERLINE
    }

}

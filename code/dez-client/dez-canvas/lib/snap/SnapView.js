class SnapView {

    constructor (svgNode, zoom) {
        this._margins = new Margins (25, 25);
        this._viewBox = new ViewBox(500, 200 + this._margins.totalSize());
        this._zoom = zoom
        this._paper = Snap(svgNode).attr({
            height: this._viewBox.height * zoom,
            width: this._viewBox.width * zoom,
            viewBox: this._viewBox.toString()
        });
        this._grobsGroup = this._paper.group();
        this._image;
    }

    get zoom () {
      return this._zoom
    }

    set zoom (value) {
      if (this._zoom == value) return
      this._zoom = value
      this._paper.attr({
        height: this._viewBox.height * this._zoom,
        width: this._viewBox.width * this._zoom,
        viewBox: this._viewBox.toString()
      });
    }

    fitToHeight (height) {
      let currentHeight = this._viewBox.height * this._zoom
      if (currentHeight > height) {
        this.zoom = height / currentHeight
      }
    }
    // convert x
    x (x) {
        let svgLeft = this._paper.node.getBoundingClientRect().left
        return (x - svgLeft) / this._zoom;
    }

    // convert y
    y (y) {
        let svgTop = this._paper.node.getBoundingClientRect().top
        return (y - svgTop) / this._zoom - this._margins.top;
    }

    // convert length
    length (length) { return length / this._zoom }

    get height () {
        return this._viewBox.height - this._margins.totalSize()
    }

    set height (height) {
        this._viewBox.height = height + this._margins.totalSize();
        this._paper.attr({
            height: this._viewBox.height * this._zoom,
            viewBox: this._viewBox.toString()
        });
    }

    set width (width) {
        this._viewBox.width = width;
        this._paper.attr({
            width: this._viewBox.width * this._zoom,
            viewBox: this._viewBox.toString()
        });

    }

    setImage (src, height) {
        if (this._image) this._image.remove();
        this._image = this._paper.image(
            src,
            0,
            this._margins.top,
            this._viewBox.width,
            height
        );
        this._grobsGroup.before(this._image);
    }

    shiftImage (height) {
        if (this._image == undefined) return
        let top = parseInt(this._image.attr("y")) + height;
        this._image.attr({"y": top});
    }

    fitToImage () {
        if (this._image) {
            this._image.attr({"y": this._margins.top});
            this.height = parseInt(this._image.attr("height"));
        } else {
            this.width = 500;
            this.height = 200;
        }
    }

    setCursorOn (x) {
      if (!this._cursor) {
          // this._cursor = this._paper.rect(x, this._margins.top, 2, this.height)
          this._cursor = this._paper.rect(x-5, this._margins.top, 10, 30).attr({ opacity: .3 })
      } else {
          // this._cursor.attr({"x": x, "height": this.height})
          this._cursor.attr({"x": x-5})
      }
    }

    deleteCursor () {
      if (this._cursor) {
        this._cursor.remove()
        this._cursor = undefined
      }
    }

    // creates a SnapRectangle
    createRectangle (x, y, width, height, color, opacity) {
        let rect = this._paper.rect(
            x,
            y + this._margins.top,
            width,
            height,
            10
        ).attr({
            fill: color,
            opacity: opacity,
            stroke: "black",
            strokeWidth:0,
        })
        this._grobsGroup.add(rect);
        return new SnapRectangle(rect, this._margins.top)
    }

    // creates a SnapText
    createText (text, styleClass) {
        let textView = this._paper.text(
            -100, 0, text
        ).attr(
            {class: styleClass}
        );
        this._grobsGroup.add(textView)
        return new SnapText(textView, 2 + this._margins.top);
    }

    // creates a SnapTriangle
    createTriangle (x, y, width, height, direction, color, opacity) {
        let polygon = this._paper.polygon(
        ).attr({
             fill: color,
             opacity: opacity,
             stroke: "black",
             strokeWidth:0,
        })
        this._grobsGroup.add(polygon);
        return new SnapTriangle(polygon, x, y, width, height, direction, this._margins.top)
    }

    createResizeDecorator (direction, point) {
        let element = this._paper.path();
        this._grobsGroup.add(element);
        return new SnapResizeDecorator(direction, element, point);
    }

}

class ViewBox {

    constructor (width, height) {
        this._width = width;
        this._height = height;
    }

    get width () { return this._width }
    set width (width) { this._width = width }
    get height () { return this._height }
    set height (height) { this._height = height }

    toString () {
        return "0 0 " + this.width + " " + this.height
    }

}

class Margins {

    constructor (top, bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    totalSize () {
        return this.top + this.bottom
    }

}

class GrobFactory {

    constructor (mediator, grid) {
        this._counter = 0;
        this._mediator = mediator;
        this._grid = grid;
        this._grobs = {}
    }

    create (x, width, shape, useGrid = false) {
        let ret = new Grob(x, width, shape, this._mediator, ++this._counter);
        ret.grid = this._grid;
        ret.refresh(useGrid)
        this._grobs[this._counter] = ret;
        return ret;
    }

    set grid (grid) { this._grid = grid; }

    get (id) {
        return this._grobs[id];
    }

    /**
     * Remove all references of the grob with this id.
     * @param  {[Number} id id of the grob to remove
     */
    remove (id) {
	if(this._grobs[id]==undefined){
		return
	}
        this._mediator.remove(id);
        this._grobs[id].remove();
        delete this._grobs[id];
    }

    removeAll () {
        Object.keys(this._grobs).forEach(id => this.remove(id));
    }

}
